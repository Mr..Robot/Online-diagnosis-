@extends('layouts.main')

@section('content')

    <div class="container-fluid mt--7">
      <div class="row">
        
            <div class="col">
                    <div class="card shadow">
                        @include('includes.messages')
                      
                          <div>
                              <p class="main-title">إدارة الشهادات | </p>
                              <p class="smale-title">قائمة الشهادات</p>
                          </div>
                      <div class="row">
                        <div>
                          <a class="btn btn-success" href="{{ route('doctor_certificates.create') }}"> إضافة</span></a>
                        </div>
                      </div>
                      <br>
                      <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                          <thead class="thead-light">
                            <tr>
                              <th>#رقم</th>
                              <th width="30%">اسم الشهادة</th>
                              <th>العمليات</th>
                            </tr>
                          </thead>
                          <tbody style="text-align: center">
                            @foreach (Auth::user()->certificates as $index => $certificate)
                              <tr>
                                <td> {{ $index + 1 }} </td>
                                <td> {{ $certificate->name }} </td>
                                <td>
                                    <a class="btn btn-info  btn-sm" href="{{ $certificate->link}}"><span class="fa fa-eye"></span>عرض </a>
                                  <a class="btn btn-primary  btn-sm" href="{{ route('doctor_certificates.edit', $certificate->id)}}"><span class="fa fa-edit"></span>تعديل </a>
                                  <a class="btn btn-danger btn-sm remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('doctor_certificates.destroy', $certificate->id) }}" data-id="{{$certificate->id}}" data-target="#custom-width-modal"><span class="fa fa-trash"></span>حذف</a>
                                </td>
                              </tr>  
                            @endforeach
          
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
    </div>
</div>
@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
    {{-- <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script> --}}
    {{-- <script src="{{asset('js/jquery.dataTables.min.js')}}"></script> --}}

    {{-- <script>
      $(document).ready( function () {
          $('#myTable').DataTable();
      });
    </script> --}}

@endsection
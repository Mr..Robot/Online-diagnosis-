@extends('layouts.main')

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
                <div class="col">
                        <div class="card shadow">
                            <div>
                                    @include('includes.messages')
                      
                                    <div>
                                            <p class="main-title">إدارة الشهادات | </p>
                                            <p class="smale-title">تعديل شهادة</p>
                                    </div>
                        <div class="bord">
                            <form method="POST" action="{{ route('doctor_certificates.update', $certificate->id) }}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group focused">
                                            <label class="form-control-label " >اسم العرض</label>
                                            <input type="text" name="name" value="{{ $certificate->name }}" class="form-control form-control-alternative" placeholder="اسم العرض">
                                        </div>
                                    </div>
                                </div>
                              
                                <div class="row">
                                    <div class=" col-md-12 ">
                                        <button type="submit" class="btn btn-success">
                                            حفظ <i class="fa fa-check"></i>
                                        </button>
                                        <a href="{{route('doctor_certificates.index')}}" class="btn btn-danger">   إلغاء الأمر <i class="fa fa-close"></i> </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
    </div>
</div>
@endsection
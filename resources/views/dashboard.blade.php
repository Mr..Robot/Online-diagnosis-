<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Design System for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Argon Design System - Free Design System for Bootstrap 4</title>
  <!-- Favicon -->
  <link href="{{ asset('website/img/brand/favicon.png')}}" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('website/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
  <link href="{{ asset('website/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('website/css/argon.css?v=1.0.1')}}" rel="stylesheet">
  <!-- Docs CSS -->
  <link type="text/css" href="{{ asset('website/css/docs.min.css')}}" rel="stylesheet">
  <link type="text/css" href="{{ asset('template/css/custome.css')}}" rel="stylesheet">
      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="{{ asset('css/s.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/tags.css') }}" rel="stylesheet" />
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
</head>

<body >
  <header class="header-global">
    <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom">
      <div class="container">
        <a class="navbar-brand mr-lg-5" href="#">
          <img src="{{ asset('website/img/brand/white.png')}}">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbar_global">
          <div class="navbar-collapse-header">
            <div class="row">
              <div class="col-6 collapse-brand">
                <a href="./index.html">
                  <img src="{{ asset('website/assets/img/brand/blue.png')}}">
                </a>
              </div>
              <div class="col-6 collapse-close">
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                  <span></span>
                  <span></span>
                </button>
              </div>
            </div>
          </div>
          @if(Auth::guest())
          <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
            <li class="nav-item dropdown">
              <a href="{{ route('login')}}" class="nav-link" role="button">
                <i class="ni ni-ui-04 d-lg-none"></i>
                <span class="customer-font nav-link-inner--text">تسجيل الدخول</span>
              </a>
            </li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
                  <i class="ni ni-collection d-lg-none"></i>
                  <span class="customer-font nav-link-inner--text">إنشاء حساب</span>
                </a>
                <div class="dropdown-menu">
                  <a href="{{ route('register.create') }}" class="dropdown-item">كمستخدم</a>
                  <a href="{{ route('register_doctor') }}" class="dropdown-item">كطبيب</a>
                </div>
              </li>
          </ul>
          @endif
        </div>
      </div>
    </nav>
  </header>
  <main>
    <div class="position-relative">
      <!-- Hero for FREE version -->
      <section class="section section-lg section-hero section-shaped">
        <!-- Background circles -->
        <div class="shape shape-style-1 shape-primary">
          <span class="span-200"></span>
          <span class="span-50"></span>
          <span class="span-50"></span>
          <span class="span-75"></span>
          <span class="span-100"></span>
          <span class="span-75"></span>
          <span class="span-50"></span>
          <span class="span-100"></span>
          <span class="span-50"></span>
          <span class="span-100"></span>
        </div>
        <div class="container shape-container d-flex align-items-center py-lg">
          <div class="col px-0">
            <div class="row align-items-center justify-content-center">
              <div class="col-lg-6 text-center">
                <img src="{{ asset('website/img/brand/white.png')}}" style="width: 200px;" class="img-fluid">
                <p class="lead text-white">تطبيق البحث عن أفضل الأخصائيين</p>
                <div class="btn-wrapper mt-5">
                  
                
                    <a href="{{ route('search.view') }}" class="btn btn-lg btn-white btn-icon mb-3 mb-sm-0">
                        <span class="btn-inner--icon"><i class="ni ni-user"></i></span>
                        <span class="btn-inner--text">بحث عن افضل اخصائي</span>
                    </a>
                    </div>
                <div class="mt-5">
                  {{-- <img src="./assets/img/brand/creativetim-white-slim.png" style="height: 28px;"> --}}
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- SVG separator -->
        <div class="separator separator-bottom separator-skew zindex-100">
          <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
          </svg>
        </div>
      </section>
    </div>
    <section class="section section-lg section-nucleo-icons pb-250">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
              <h2 class="display-3">كل التخصصات</h2>
              <p class="lead">
                أطفال - عيون - جراحة - باطنة - قلب والمزيد 
              </p>
            </div>
          </div>
          <div class="blur--hover">
            <a href="#">
              <div class="icons-container blur-item mt-5" data-toggle="on-screen">
                <!-- Center -->
                <i class="icon ni ni-diamond"></i>
                <!-- Right 1 -->
                <i class="icon icon-sm ni ni-album-2"></i>
                <i class="icon icon-sm ni ni-app"></i>
                <i class="icon icon-sm ni ni-atom"></i>
                <!-- Right 2 -->
                <i class="icon ni ni-bag-17"></i>
                <i class="icon ni ni-bell-55"></i>
                <i class="icon ni ni-credit-card"></i>
                <!-- Left 1 -->
                <i class="icon icon-sm ni ni-briefcase-24"></i>
                <i class="icon icon-sm ni ni-building"></i>
                <i class="icon icon-sm ni ni-button-play"></i>
                <!-- Left 2 -->
                <i class="icon ni ni-calendar-grid-58"></i>
                <i class="icon ni ni-camera-compact"></i>
                <i class="icon ni ni-chart-bar-32"></i>
              </div>
              <span class="blur-hidden h5 text-success"></span>
            </a>
          </div>
        </div>
      </section>
   

    <section class="section section-shaped">
        <div class="shape shape-style-1 shape-default">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div class="container py-md">
          <div class="row justify-content-between align-items-center">
            <div class="col-lg-5 mb-5 mb-lg-0">
              <h1 class="text-white font-weight-light">منصة للكشوفات</h1>
              <p class="lead text-white mt-4">منصة للبحث عن أفضل الأخصائيين والأطباء في كل التخصصات</p>
              <a href="#" class="btn btn-white mt-4">سجل الآن</a>
            </div>
            <div class="col-lg-6 mb-lg-auto">
              <div class="rounded shadow-lg overflow-hidden transform-perspective-right">
                <div id="carousel_example" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carousel_example" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel_example" data-slide-to="1"></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img class="img-fluid" src="{{ asset('website/img/theme/img-1-1200x1000.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                      <img class="img-fluid" src="{{ asset('website/img/theme/img-2-1200x1000.jpg')}}" alt="Second slide">
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carousel_example" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carousel_example" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- SVG separator -->
        <div class="separator separator-bottom separator-skew">
          <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
          </svg>
        </div>
      </section>

    <section class="section section-lg">
      <div class="container">
        <div class="row row-grid justify-content-center">
          <div class="col-lg-8 text-center">
            <h2 class="display-3">أشترك معنا لتستفيد
              <span class="text-success">لا تتردد !!</span>
            </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section section-lg section-shaped">
        <div class="shape shape-style-1 shape-default">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
        <!-- SVG separator -->
        <div class="separator separator-bottom separator-skew">
          <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
          </svg>
        </div>
      </section>
  </main>
  <footer class="footer has-cards">
    <div class="container container-lg">
    </div>
    <div class="container">
      <div class="row row-grid align-items-center my-md">
        <div class="col-lg-6">
          <h3 class="text-primary font-weight-left mb-2">شكراً لزيارتك</h3>
          <h4 class="mb-0 font-weight-light">تابعنا على منصات التواصل الأجتماعي</h4>
        </div>
        <div class="col-lg-6 text-lg-center btn-wrapper">
          <a target="_blank" href="#" class="btn btn-neutral btn-icon-only btn-twitter btn-round btn-lg" data-toggle="tooltip" data-original-title="Follow us">
            <i class="fa fa-twitter"></i>
          </a>
          <a target="_blank" href="#" class="btn btn-neutral btn-icon-only btn-facebook btn-round btn-lg" data-toggle="tooltip" data-original-title="Like us">
            <i class="fa fa-facebook-square"></i>
          </a>
          <a target="_blank" href="#" class="btn btn-neutral btn-icon-only btn-dribbble btn-lg btn-round" data-toggle="tooltip" data-original-title="Follow us">
            <i class="fa fa-dribbble"></i>
          </a>
          <a target="_blank" href="#" class="btn btn-neutral btn-icon-only btn-github btn-round btn-lg" data-toggle="tooltip" data-original-title="Star on Github">
            <i class="fa fa-github"></i>
          </a>
        </div>
      </div>
      <hr>
      <div class="row align-items-center justify-content-md-between">
        <div class="col-md-6">
          <div class="copyright">
            &copy; 2018
            <a href="#" target="_blank">البحث عن أفضل اخصائي</a>.
          </div>
        </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Core -->
  <script src="{{ asset('js/jquery.min.js')}}"></script>
  <script src="{{ asset('website/vendor/popper/popper.min.js')}}"></script>
  <script src="{{ asset('website/vendor/bootstrap/bootstrap.min.js')}}"></script>
  <script src="{{ asset('website/vendor/headroom/headroom.min.js')}}"></script>
  <!-- Optional JS -->
  <script src="{{ asset('website/vendor/onscreen/onscreen.min.js')}}"></script>
  <script src="{{ asset('website/vendor/nouislider/js/nouislider.min.js')}}"></script>
  <script src="{{ asset('website/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{ asset('website/js/argon.js?v=1.0.1')}}"></script>
  <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

  {{-- <script src="{{ asset('js/jquery.min.js')}}"></script> --}}
  {{-- <script src="{{ asset('js/lodash.min.js')}}"></script> --}}
  <script src="{{ asset('js/j.js')}}"></script>

  {{-- <script>
   $(document).ready(function() {
            var jsonData = [];
            var fruits = '{{ $symptoms }}'.split(',');
            for(var i=0;i<fruits.length;i++) jsonData.push({id:i,name:fruits[i]});
            var ms1 = $('#ms1').tagSuggest({
                data: jsonData,
                sortOrder: 'name',
                maxDropHeight: 200,
                name: 'ms1'
            });
        });
  </script> --}}

  <script>
      $(document).ready( function () {
        oTable = $('#mytable').DataTable({
          "bPaginate": false,
          "bInfo" : false,
          "language": {
            "search": ""
          }
        });
        $('#myInputTextField').keyup(function(){
              oTable.search($(this).val()).draw() ;
        })
    } );

    
  </script>

  
  {{-- <script>

 // Events
    $('.dropdown-container')
        .on('click', '.dropdown-button', function () {
            $(this).siblings('.dropdown-list').toggle();
        })
        .on('input', '.dropdown-search', function () {
            var target = $(this);
            var dropdownList = target.closest('.dropdown-list');
            var search = target.val().toLowerCase();

            if (!search) {
                dropdownList.find('li').show();
                return false;
            }

            dropdownList.find('li').each(function () {
                var text = $(this).text().toLowerCase();
                var match = text.indexOf(search) > -1;
                $(this).toggle(match);
            });
        })
        .on('change', '[type="checkbox"]', function () {
            var container = $(this).closest('.dropdown-container');
            var numChecked = container.find('[type="checkbox"]:checked').length;
            container.find('.quantity').text(numChecked || 'Any');
        });

    // JSON of States for demo purposes
    var usStates = [
        { name: 'ALABAMA', abbreviation: 'AL' },
        { name: 'ALASKA', abbreviation: 'AK' },
        { name: 'AMERICAN SAMOA', abbreviation: 'AS' },
        { name: 'ARIZONA', abbreviation: 'AZ' },
        { name: 'ARKANSAS', abbreviation: 'AR' },
        { name: 'CALIFORNIA', abbreviation: 'CA' },
        { name: 'COLORADO', abbreviation: 'CO' },
        { name: 'CONNECTICUT', abbreviation: 'CT' },
        { name: 'DELAWARE', abbreviation: 'DE' },
        { name: 'DISTRICT OF COLUMBIA', abbreviation: 'DC' },
        { name: 'FEDERATED STATES OF MICRONESIA', abbreviation: 'FM' },
        { name: 'FLORIDA', abbreviation: 'FL' },
        { name: 'GEORGIA', abbreviation: 'GA' },
        { name: 'GUAM', abbreviation: 'GU' },
        { name: 'HAWAII', abbreviation: 'HI' },
        { name: 'IDAHO', abbreviation: 'ID' },
        { name: 'ILLINOIS', abbreviation: 'IL' },
        { name: 'INDIANA', abbreviation: 'IN' },
        { name: 'IOWA', abbreviation: 'IA' },
        { name: 'KANSAS', abbreviation: 'KS' },
        { name: 'KENTUCKY', abbreviation: 'KY' },
        { name: 'LOUISIANA', abbreviation: 'LA' },
        { name: 'MAINE', abbreviation: 'ME' },
        { name: 'MARSHALL ISLANDS', abbreviation: 'MH' },
        { name: 'MARYLAND', abbreviation: 'MD' },
        { name: 'MASSACHUSETTS', abbreviation: 'MA' },
        { name: 'MICHIGAN', abbreviation: 'MI' },
        { name: 'MINNESOTA', abbreviation: 'MN' },
        { name: 'MISSISSIPPI', abbreviation: 'MS' },
        { name: 'MISSOURI', abbreviation: 'MO' },
        { name: 'MONTANA', abbreviation: 'MT' },
        { name: 'NEBRASKA', abbreviation: 'NE' },
        { name: 'NEVADA', abbreviation: 'NV' },
        { name: 'NEW HAMPSHIRE', abbreviation: 'NH' },
        { name: 'NEW JERSEY', abbreviation: 'NJ' },
        { name: 'NEW MEXICO', abbreviation: 'NM' },
        { name: 'NEW YORK', abbreviation: 'NY' },
        { name: 'NORTH CAROLINA', abbreviation: 'NC' },
        { name: 'NORTH DAKOTA', abbreviation: 'ND' },
        { name: 'NORTHERN MARIANA ISLANDS', abbreviation: 'MP' },
        { name: 'OHIO', abbreviation: 'OH' },
        { name: 'OKLAHOMA', abbreviation: 'OK' },
        { name: 'OREGON', abbreviation: 'OR' },
        { name: 'PALAU', abbreviation: 'PW' },
        { name: 'PENNSYLVANIA', abbreviation: 'PA' },
        { name: 'PUERTO RICO', abbreviation: 'PR' },
        { name: 'RHODE ISLAND', abbreviation: 'RI' },
        { name: 'SOUTH CAROLINA', abbreviation: 'SC' },
        { name: 'SOUTH DAKOTA', abbreviation: 'SD' },
        { name: 'TENNESSEE', abbreviation: 'TN' },
        { name: 'TEXAS', abbreviation: 'TX' },
        { name: 'UTAH', abbreviation: 'UT' },
        { name: 'VERMONT', abbreviation: 'VT' },
        { name: 'VIRGIN ISLANDS', abbreviation: 'VI' },
        { name: 'VIRGINIA', abbreviation: 'VA' },
        { name: 'WASHINGTON', abbreviation: 'WA' },
        { name: 'WEST VIRGINIA', abbreviation: 'WV' },
        { name: 'WISCONSIN', abbreviation: 'WI' },
        { name: 'WYOMING', abbreviation: 'WY' }
    ];

    // <li> template
    var stateTemplate = _.template(
        '<li>' +
        '<input name="<%= abbreviation %>" type="checkbox">' +
        '<label for="<%= abbreviation %>"><%= capName %></label>' +
        '</li>'
    );

    // Populate list with states
    _.each(usStates, function (s) {
        s.capName = _.startCase(s.name.toLowerCase());
        $('ul').append(stateTemplate(s));
    });


  </script>
 --}}  
 <script>
  
      $(document).ready(function() {
         $('#serch').hide();
        $('input:checkbox[name=s]').change(function() 
        {    
            if($(this).is(':checked')){
                // $('#range-picker').hide();
                $('#serch').show();
            } else {
               $('#serch').hide();
            }
             
        });
      });

  </script>

</body>

</html>
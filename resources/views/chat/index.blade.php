@extends('layouts.main')
@section('styles')
 
@endsection
@section('content') 

    <div class="container-fluid mt--7">
      <div class="row">
        
        <div class="col">
          <div class="card shadow">
              @include('includes.messages')
            
                <div>
                    <p class="main-title">المراسلات | </p>
                    <p class="smale-title">قائمة المراسلات</p>
                </div>

            <br>
            <div class="table-responsive">
              <table  id="myTable" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th></th>
                    <th width="30%"></th>
                    <th width="30%"></th>
                    <th width="30%"></th>
                    <th width="20%"></th>
                    <th> </th>
                  </tr>
                </thead>
                <tbody style="text-align: center">
                  @foreach ($chats as $index => $chat)
                    <tr>
                      <td> {{ $chat->id }} </td>
                      <td> {{ $chat->sender->name }} </td>
                      <td> {{ $chat->recived->name }} </td>
                      <td>
                        <a class="btn btn-info " href="{{ route('chats.edit', $chat->id)}}"><span class="fa fa-eye"></span> </a>
                      </td> 
                    </tr>  
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection
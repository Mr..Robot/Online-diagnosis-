@extends('layouts.main')
@section('styles')
<style>
.chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}

.chat li
{
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
}

.chat li.left .chat-body
{
    margin-left: 60px;
}

.chat li.right .chat-body
{
    margin-right: 60px;
}


.chat li .chat-body p
{
    margin: 0;
    color: #777777;
}

.panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}
::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

::-webkit-scrollbar
{
    width: 12px;
    background-color: #F5F5F5;
}

::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
}

.panel-body {
    padding: 20px;
    overflow-y: scroll;
    height: 350px;
}

.right-box{
    color: white;
    background-color: #2fc9e6;
    padding: 10px;
}
.left-box {
    color: white;
    background-color: #e62fb0;
    padding: 10px;
}
.text-right{
    text-align: right
}
</style>
@endsection
@section('content') 
<br>
<div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
                    <ul class="chat">      
                        @foreach ($messages as $message)
                           <li class="right clearfix">
                                <span class="chat-img pull-right">
                                    <div class="{{ Auth::user()->id == $message->user_id ? 'right-box' : 'left-box' }}">{{ Auth::user()->id == $message->user_id ? 'أنت' : 'هو' }}</div>
                                </span>
                                <div class="chat-body ">
                                    <div class="header">
                                        <small class=" text-muted"><span class="glyphicon glyphicon-time">{{ $message->date }}</span></small>
                                        <strong class="pull-right primary-font"> {{ $message->user->name }}</strong>
                                    </div>
                                    <br>
                                    <p class="text-right">
                                        {{ $message->text }}
                                    </p>
                                </div>
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>
            <br>
            <form method="POST" action="{{ route('chats.store') }}">
            @csrf
                <div class="input-group">
                    <div class = " col-md-1">
                        <p>رسالة</p>
                    </div>
                    <div class = " col-md-10">
                        <input name="message" type="text" class="form-control " placeholder="أضغط للكتابة" />
                        <input type="hidden" value="{{ $id }}" name="chat_id" />
                    </div>
                    <div class = " col-md-1">
                        <button type="submit" class="btn btn-success">
                            أرسل <i class="fa fa-check"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
    {{-- <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script> --}}
    {{-- <script src="{{asset('js/jquery.dataTables.min.js')}}"></script> --}}

    {{-- <script>
      $(document).ready( function () {
          $('#myTable').DataTable();
      });
    </script> --}}
@endsection
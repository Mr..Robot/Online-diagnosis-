@extends('layouts.main')

@section('content')
    <div class="container-fluid mt--7">
      <div class="row">
        
        <div class="col">
          <div class="card shadow">
              @include('includes.messages')
            
                <div>
                    <p class="main-title">إدارة الجنسيات | </p>
                    <p class="smale-title">قائمة الجنسيات</p>
                </div>
            <div class="row">
              <div>
                <a class="btn btn-primary" href="{{ route('nationalities.create') }}"> إضافة</span></a>
              </div>
            </div>
            <br>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>#رقم</th>
                    <th width="90%" style="text-align: center">الجنسية</th>
                    <th>العمليات</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($nationalitits as $index => $nationality)
                    <tr>
                      <td> {{ $index + 1 }} </td>
                      <td style="text-align: center"> {{ $nationality->nationality_name }} </td>
                      <td>
                        <a class="btn btn-primary  btn-sm" href="{{ route('nationalities.edit', $nationality->id)}}"><span class="fa fa-edit"></span> تعديل</a>
                          <a class="btn btn-danger remove-record btn-sm" data-csrf="{{ csrf_token() }}" data-toggle="modal" data-url="{{route('nationalities.destroy', $nationality->id) }}" data-id="{{$nationality->id}}" data-target="#custom-width-modal"> <span class="fa fa-trash"></span> حذف</a>
  
                      </td>
                    </tr>  
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
    {{-- <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script> --}}
    {{-- <script src="{{asset('js/jquery.dataTables.min.js')}}"></script> --}}

    {{-- <script>
      $(document).ready( function () {
          $('#myTable').DataTable();
      });
    </script> --}}

@endsection
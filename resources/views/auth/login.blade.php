<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Argon Dashboard - Free Dashboard for Bootstrap 4</title>
  <!-- Favicon -->
  <link href="{{ asset('template/img/brand/favicon.png')}}" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('template/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
  <link href="{{ asset('template/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
  <link type="text/css" href="{{asset('template/css/custome.css')}}" rel="stylesheet">
  
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('template/css/argon.css?v=1.0.0')}}" rel="stylesheet">
  <link type="text/css" href="{{asset('template/css/reg.css')}}" rel="stylesheet">
</head>

<body dir="rtl" class="body-custome bg-default">
  <div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
      <div class="container px-4">
        <a class="navbar-brand" href="../index.html">
          <img src="../assets/img/brand/white.png" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
    </nav>
    
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6">
              <h1 class="text-white">مرحباً</h1>
              <p class="text-lead text-light">إدخل البيانات لتسجيل الدخول </p>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
          
        </svg>
      </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <!-- Table -->
      <div class="row justify-content-center">
        <div class="col-lg-8 col-md-8">
          <div class="card bg-secondary shadow border-0">
            <div class="card-body px-lg-5 py-lg-5">

                <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="form-group focused {{ $errors->has('email') ? ' has-danger' : '' }}">
                            <input type="email" name="email" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} " placeholder="البريد الإلكتروني">
                            @if ($errors->has('email'))
                                <span class="is-invalid" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12 ">
                        <div class="form-group focused {{ $errors->has('password') ? ' has-danger' : '' }}">
                            <input type="password" name="password" value="{{ old('password') }}" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }} " placeholder="كلمة المرور">
                            @if ($errors->has('password'))
                                <span class="is-invalid" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-lg-9 ">
                    <button type="submit" class="btn btn-success">
                        دخول
                    </button>
                  </div>
                  <div class="col-md-3">
                    <a href="{{ route('register.index') }}" class="nav-link" role="button">
                        <i class="ni ni-ui-04 d-lg-none"></i>
                        <span class="customer-font nav-link-inner--text"> <i class="fa fa-user"></i> إنشاء حساب</span>
                    </a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="py-5">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
        </div>
      </div>
    </div>
  </footer>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{ asset('template/assets/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{ asset('template/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{ asset('template/assets/js/argon.js?v=1.0.0')}}"></script>
</body>

</html>

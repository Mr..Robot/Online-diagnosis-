<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Argon Dashboard - Free Dashboard for Bootstrap 4</title>
  <!-- Favicon -->
  <link href="{{ asset('template/img/brand/favicon.png')}}" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('template/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
  <link href="{{ asset('template/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
  <link type="text/css" href="{{asset('template/css/custome.css')}}" rel="stylesheet">
  
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('template/css/argon.css?v=1.0.0')}}" rel="stylesheet">
  <link type="text/css" href="{{asset('template/css/reg.css')}}" rel="stylesheet">
</head>

<body dir="rtl" class="body-custome bg-default">
  <div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
      <div class="container px-4">
        <a class="navbar-brand" href="../index.html">
          <img src="../assets/img/brand/white.png" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
    </nav>
    
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6">
              <h1 class="text-white">مرحباً</h1>
              <p class="text-lead text-light">تسجيل حساب طبيب / أخصائي </p>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
          
        </svg>
      </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <!-- Table -->
      <div class="row justify-content-center">
        <div class="col-lg-8 col-md-8">
          <div class="card bg-secondary shadow border-0">
            <div class="card-body px-lg-5 py-lg-5">

                <form action="{{route('store_doctor')}}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="form-group focused {{ $errors->has('first_name') ? ' has-danger' : '' }}">
                            <label for="">الاسم الأول</label>

                            <input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }} " placeholder="الاسم الأول">
                            @if ($errors->has('first_name'))
                                <span class="is-invalid" role="alert">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12 ">
                        <div class="form-group focused {{ $errors->has('mid_name') ? ' has-danger' : '' }}">
                            <label for="">اسم الاب</label>

                            <input type="text" name="mid_name" value="{{ old('mid_name') }}" class="form-control {{ $errors->has('mid_name') ? ' is-invalid' : '' }} " placeholder="اسم الأب">
                            @if ($errors->has('mid_name'))
                                <span class="is-invalid" role="alert">
                                    <strong>{{ $errors->first('mid_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="form-group focused {{ $errors->has('last_name') ? ' has-danger' : '' }}">
                            <label for="">اسم الجد</label>

                            <input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }} " placeholder="اسم الجد">
                            @if ($errors->has('last_name'))
                                <span class="is-invalid" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12 ">
                        <div class="form-group focused {{ $errors->has('family_name') ? ' has-danger' : '' }}">
                                <label for="">اللقب</label>

                            <input type="text" name="family_name" value="{{ old('family_name') }}" class="form-control {{ $errors->has('family_name') ? ' is-invalid' : '' }} " placeholder="اللقب">
                            @if ($errors->has('family_name'))
                                <span class="is-invalid" role="alert">
                                    <strong>{{ $errors->first('family_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                  <div class="col-lg-12 ">
                      <div class="form-group focused {{ $errors->has('date_of_birth') ? ' has-danger' : '' }}">
                                <label for="">تاريخ الميلاد</label>

                          <input type="date" name="date_of_birth" value="{{ old('date_of_birth') }}" class="form-control {{ $errors->has('date_of_birth') ? ' is-invalid' : '' }} " placeholder="تاريخ الميلاد">
                          @if ($errors->has('date_of_birth'))
                              <span class="is-invalid" role="alert">
                                  <strong>{{ $errors->first('date_of_birth') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                      <div class="col-lg-12 ">
                          <div class="form-group focused {{ $errors->has('nationality_id') ? ' has-danger' : '' }}">
                                    <label for="">الجنسية</label>

                              <select class="form-control {{ $errors->has('nationality_id') ? ' is-invalid' : '' }}" name="nationality_id">
                                  <option value="-1">الجنسية</option>
                                  @foreach ($nationalities as $nationality)
                                      <option value="{{ $nationality->id }}" {{ old('nationality_id') == $nationality->id ? 'selected': '' }}>{{ $nationality->nationality_name }}</option>
                                  @endforeach
                              </select>      
                              @if ($errors->has('nationality_id'))
                                  <span class="is-invalid" role="alert">
                                      <strong>{{ $errors->first('nationality_id') }}</strong>
                                  </span>
                              @endif                          
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-lg-12 ">
                          <div class="form-group focused {{ $errors->has('email') ? ' has-danger' : '' }}">
                                <label for="">البريد الإلكتروني</label>

                              <input type="email" name="email" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} " placeholder="البريد الإلكتروني">
                              @if ($errors->has('email'))
                                  <span class="is-invalid" role="alert">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                      <div class="col-lg-12 ">
                          <div class="form-group focused {{ $errors->has('password') ? ' has-danger' : '' }}">
                                <label for="">كلمة المرور</label>

                              <input type="password" name="password" value="{{ old('password') }}" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }} " placeholder="كلمة المرور">
                              @if ($errors->has('password'))
                                  <span class="is-invalid" role="alert">
                                      <strong>{{ $errors->first('password') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                      <div class="col-lg-12 ">
                          <div class="form-group focused {{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                            <label for="">تأكيد كلمة المرور</label>

                              <input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }} " placeholder="تأكيد كلمة المرور">
                              @if ($errors->has('password_confirmation'))
                                  <span class="is-invalid" role="alert">
                                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                  </div>

                    <div class="col-lg-12 ">
                      <div class="form-group focused ">
                            <label for="">الجنس</label>

                            <select class="form-control {{ $errors->has('gender') ? ' has-danger' : '' }}" name="gender">
                                <option value="-1">أختر الجنس</option>
                                <option value="0"  {{ old('gender') == 0 ? 'selected': '' }}>ذكر</option>
                                <option value="1"  {{ old('gender') == 1 ? 'selected': '' }}>أنثى</option>
                            </select>      
                            @if ($errors->has('gender'))
                                <span class="is-invalid" role="alert">
                                    <strong>{{ $errors->first('gender') }}</strong>
                                </span>
                          @endif                          
                      </div>
                    </div>

                    <div class="col-lg-12 ">
                        <div class="form-group focused {{ $errors->has('ref_no') ? ' has-danger' : '' }}">
                            <label for="">الرقم النقابي</label>

                            <input type="text" name="ref_no" value="{{ old('ref_no') }}" class="form-control {{ $errors->has('ref_no') ? ' is-invalid' : '' }} " placeholder="الرقم النقابي">
                            @if ($errors->has('ref_no'))
                                <span class="is-invalid" role="alert">
                                    <strong>{{ $errors->first('ref_no') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-lg-12 ">
                        <div class="form-group focused {{ $errors->has('dgree') ? ' has-danger' : '' }}">
                                <label for="">الدرجة</label>

                            <input type="text" name="dgree" value="{{ old('dgree') }}" class="form-control {{ $errors->has('dgree') ? ' is-invalid' : '' }} " placeholder="الدرجة">
                            @if ($errors->has('dgree'))
                                <span class="is-invalid" role="alert">
                                    <strong>{{ $errors->first('dgree') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12 ">
                        <div class="form-group">
                            <label class="form-control-label">صورة المؤهل العلمي</label>
                            <div class="form-control"> 
                                <input type="file" name="cover_image" id="cover_image">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-9 ">
                            <button type="submit" class="btn btn-success">
                                إنشاء حساب
                            </button>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ route('login') }}" class="nav-link" role="button">
                                <i class="ni ni-ui-04 d-lg-none"></i>
                                <span class="customer-font nav-link-inner--text"> <i class="fa fa-user"></i> تسجيل دخول</span>
                            </a>
                        </div>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="py-5">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
        </div>
      </div>
    </div>
  </footer>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{ asset('template/assets/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{ asset('template/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{ asset('template/assets/js/argon.js?v=1.0.0')}}"></script>
</body>

</html>
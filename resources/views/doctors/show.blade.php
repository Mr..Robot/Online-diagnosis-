@extends('layouts.main')
@section('styles')
    {{-- <link rel="stylesheet" href="{{asset('css/jquery.dataTables.min.css')}}"> --}}
@endsection
@section('content')

    <div class="container-fluid mt--7">
      <div class="row">
        
        <div class="col">
          <div class="card shadow">
              @include('includes.messages')
            
                <div>
                    <p class="main-title">إدارة طلبات التسجيل | </p>
                    <p class="smale-title">صورة المؤهل</p>
                </div>
                
                <img class="img-thumbnail" src="/storage/cover_images/{{ $doctor->cover_image }}"  alt="">
                <br>
                <div class="col-lg-12 ">
                    <a href="{{ route('doctors.index')}}" class="btn btn-success">رجوع</a>
                </div>

            <br>
            <div class="container-fluid">
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
    {{-- <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script> --}}
    {{-- <script src="{{asset('js/jquery.dataTables.min.js')}}"></script> --}}

    {{-- <script>
      $(document).ready( function () {
          $('#myTable').DataTable();
      });
    </script> --}}

@endsection
@extends('layouts.main')
@section('styles')
    {{-- <link rel="stylesheet" href="{{asset('css/jquery.dataTables.min.css')}}"> --}}
@endsection
@section('content')

    <div class="container-fluid mt--7">
      <div class="row">
        
        <div class="col">
          <div class="card shadow">
              @include('includes.messages')
            
                <div>
                    <p class="main-title">إدارة طلبات التسجيل | </p>
                    <p class="smale-title">قائمة الطلبات</p>
                </div>

            <br>
            <div class="table-responsive">
              <table  id="myTable" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>#رقم</th>
                    <th width="30%">الاسم</th>
                    <th width="30%">البريد الإلكتروني</th>
                    <th width="20%">الجنس</th>
                    <th width="20%">تاريخ الميلاد</th>
                    <th>العمليات</th>
                  </tr>
                </thead>
                <tbody style="text-align: center">
                  @foreach ($doctors as $index => $doctor)
                    <tr>
                      <td> {{ $index + 1 }} </td>
                      <td> {{ $doctor->person->first_name }}  {{ $doctor->person->mid_name }}  {{ $doctor->person->last_name }}   {{ $doctor->person->family_name }} </td>
                      <td> {{ $doctor->email }} </td>
                      
                      <td>
                        @if ($doctor->person->gender == 0)
                        <div class="badge badge-success"> ذكر </div>
                        @else
                        <div class="badge badge-danger">أنثى</div>
                        @endif
                      </td>
                      <td> {{ $doctor->person->date_of_birth }} </td>

                      <td>
                        <a href="{{ route('doctors.show', $doctor->id) }}">المؤهل</a>
                        <a href="{{ route('doctors.activate', $doctor->id)}}" class="btn btn-success"><span class="fa fa-check"></a>
                      </td>
                    </tr>  
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
    {{-- <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script> --}}
    {{-- <script src="{{asset('js/jquery.dataTables.min.js')}}"></script> --}}

    {{-- <script>
      $(document).ready( function () {
          $('#myTable').DataTable();
      });
    </script> --}}

@endsection
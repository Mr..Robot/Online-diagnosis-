@extends('layouts.main')

@section('content')
    <div class="container-fluid mt--7">
      <div class="row">
        
        <div class="col">
          <div class="card shadow">
              @include('includes.messages')
            
                <div>
                    <p class="main-title">إدارة الأعراض | </p>
                    <p class="smale-title">قائمة الأعراض</p>
                </div>
            <div class="row">
              <div>
                <a class="btn btn-success" href="{{ route('symptoms.create') }}"> إضافة</span></a>
              </div>
            </div>
            <br>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>#رقم</th>
                    <th width="30%">اسم العرض</th>
                    <th width="30%">الاسم الشائع</th>
                    <th width="20%">الحالة</th>
                    <th>العمليات</th>
                  </tr>
                </thead>
                <tbody style="text-align: center">
                  @foreach ($symptoms as $index => $symptom)
                    <tr>
                      <td> {{ $index + 1 }} </td>
                      <td> {{ $symptom->name }} </td>
                      <td> {{ $symptom->common_name }} </td>

                      <td>
                          @if ($symptom->active == 1)
                              <div class="badge badge-success">مفعل</div>
                          @else
                              <div class="badge badge-danger">معطل</div>
                          @endif
                      </td>

                      <td>
                        @if ($symptom->active == 1)
                            <a href="{{route('symptoms.changeState', $symptom->id)}}" class="btn btn-danger btn-sm"><span class="fa fa-close"></span>تعطيل</a>
                        @else
                            <a href="{{route('symptoms.changeState', $symptom->id)}}" class="btn btn-success btn-sm"><span class="fa fa-check"></span>تفعيل</a>
                        @endif
                        <a class="btn btn-primary  btn-sm" href="{{ route('symptoms.edit', $symptom->id)}}"><span class="fa fa-edit"></span>تعديل </a>
                        <a class="btn btn-danger btn-sm remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('symptoms.destroy', $symptom->id) }}" data-id="{{$symptom->id}}" data-target="#custom-width-modal"><span class="fa fa-trash"></span>حذف</a>

                      </td>
                    </tr>  
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection
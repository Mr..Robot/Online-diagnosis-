@extends('layouts.main')

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div>
                    @include('includes.messages')
                    <p class="main-title">إدارة الأعراض | </p>
                    <p class="smale-title">إضافة عرض</p>
                </div>
                <div class="bord">
                    <form method="POST" action="{{ route('symptoms.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group focused">
                                    <label class="form-control-label " >اسم العرض</label>
                                    <input type="text" name="name" value="{{ old('name') }}" class="form-control form-control-alternative" placeholder="التخصص">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group focused">
                                    <label class="form-control-label " >الاسم الشائع</label>
                                    <input type="text" name="common_name" value="{{ old('common_name') }}" class="form-control form-control-alternative" placeholder="الاسم الشائع">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class=" col-md-12 ">
                                <button type="submit" class="btn btn-success">
                                    حفظ <i class="fa fa-check"></i>
                                </button>
                                <a href="{{route('symptoms.index')}}" class="btn btn-danger">   إلغاء الأمر <i class="fa fa-close"></i> </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
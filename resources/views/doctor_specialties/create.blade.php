@extends('layouts.main')

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div>
                    {{-- @include('includes.messages') --}}
                    <p class="main-title">إدارة تخصصات الطبيب | </p>
                    <p class="smale-title">إضافة تخصص</p>
                </div>
                <div class="bord">
                    <form method="POST" action="{{ route('doctor_specialties.store') }}">
                        @csrf

                        <div class="row">
                            <div class="col-lg-12 ">
                                <label class="form-control-label " >التخصص</label>
                                <div class="form-group focused {{ $errors->has('specialty_id') ? ' has-danger' : '' }}">
                                    <select class="form-control {{ $errors->has('specialty_id') ? ' is-invalid' : '' }}" name="specialty_id">
                                        <option value="-1">إختار تخصص</option>
                                        @foreach ($specialties as $specialty)
                                            <option value="{{ $specialty->id }}">{{ $specialty->name_ar }} | {{ $specialty->name_en }}</option>
                                        @endforeach
                                    </select>      
                                    @if ($errors->has('specialty_id'))
                                        <span class="is-invalid" role="alert">
                                            <strong>{{ $errors->first('specialty_id') }}</strong>
                                        </span>
                                    @endif                          
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-12 ">
                                <label class="form-control-label " >التاريخ</label>
                                <div class="form-group focused {{ $errors->has('date') ? ' has-danger' : '' }}">
                                    <input type="date" name="date" value="{{ old('date') }}" class="form-control  {{ $errors->has('date') ? ' is-invalid' : '' }}" placeholder="التاريخ">
                                    @if ($errors->has('date'))
                                        <span class="is-invalid" role="alert">
                                            <strong>{{ $errors->first('date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class=" col-md-12 ">
                                <button type="submit" class="btn btn-success">
                                    حفظ <i class="fa fa-check"></i>
                                </button>
                                <a href="{{route('doctor_specialties.index')}}" class="btn btn-danger">   إلغاء الأمر <i class="fa fa-close"></i> </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.main')

@section('content')

    <div class="container-fluid mt--7">
      <div class="row">
        
        <div class="col">
          <div class="card shadow">
              @include('includes.messages')
            
                <div>
                    <p class="main-title">تخصصات الطبيب | </p>
                    <p class="smale-title">{{ Auth::user()->name }}</p>
                </div>

            <br>
            <div class="row">


              <form method="POST" action="{{ route('doctor_specialties.store') }}">
                  @csrf
                  <div class="row">
                      @foreach ($specialties as $key => $specialty)
                          <div class="row my-4">
                              <div class="col-12">
                                  <div class="custom-control custom-control-alternative custom-checkbox">
                                  <input class="custom-control-input"  value="{{ $specialty->id }}" {{ in_array($specialty->id, $doctor_specialties) ? 'checked': '' }} name="specialties[]" id="specialties{{ $key }}" type="checkbox">
                                      <label class="custom-control-label" for="specialties{{ $key }}">
                                          <span class="text-muted">{{ $specialty->name_ar }} | {{ $specialty->name_en }}</span>
                                      </label>
                                  </div>
                              </div>
                          </div>
                      @endforeach
                      @if ($errors->has('specialties'))
                          <span class="is-invalid" role="alert">
                              <strong>{{ $errors->first('specialties') }}</strong>
                          </span>
                      @endif                          

                  </div>
                  
                  <div class="row my-4">
                      <div class=" col-md-12 ">
                          <button type="submit" class="btn btn-success">
                              حفظ <i class="fa fa-check"></i>
                          </button>
                      </div>
                  </div>

              </form>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
    {{-- <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script> --}}
    {{-- <script src="{{asset('js/jquery.dataTables.min.js')}}"></script> --}}

    {{-- <script>
      $(document).ready( function () {
          $('#myTable').DataTable();
      });
    </script> --}}

@endsection
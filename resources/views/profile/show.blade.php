@extends('layouts.main', ['title' => __('User Profile')])
@php use Carbon\Carbon; @endphp
@php use App\Description; @endphp
@php use App\Certificate; @endphp
@php use App\Symptom; @endphp
@php use App\DiseasesSyptom; @endphp
@section('content')
    {{-- @include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('This is your profile page. You can see the progress you\'ve made with your work and manage your projects or assigned tasks'),
        'class' => 'col-lg-7'
    ])    --}}

    <div class="container-fluid mt--7">
        <div class="row">
                <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
                        <div class="card card-profile shadow">
                            <div class="row justify-content-center">
                                <div class="col-lg-3 order-lg-2">
                                    <div class="card-profile-image">
                                        <a href="#">
                                            <img src="{{ asset('imgs/'.$person->avatar ) }}" class="rounded-circle">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                                <div class="d-flex justify-content-between">
                                    <a href="#" class="btn btn-sm btn-default float-right">{{ __('رسالة') }}</a>
                                </div>
                            </div>
                            <div class="card-body pt-0 pt-md-4">

                                <div class="text-center">
                                    <h3>
                                        {{  $user->name . ' '. $person->family_name }}<span class="font-weight-light"> - {{ Carbon::parse($person->date_of_birth)->age }}</span>
                                    </h3>
                                    <div class="h5 font-weight-300">
                                        <i class="ni location_pin mr-2"></i>{{ $person->address}}
                                    </div>
                                    <hr class="my-4" />
                                    <h3>الاعمال</h3>
                                    <div class="h5 mt-4">
                                            @foreach ($user->work as $item)
                                            <div>
                                               <i class="ni education_hat mr-2"></i>{{  $item->name}}
                                           </div> 
                                           @endforeach
                                        
                                    </div>
                                    <hr class="my-4" />
                                    <h3>التخصصات</h3>
                                    @foreach ($user->specialties as $item)
                                     <div>
                                        <i class="ni education_hat mr-2"></i>{{  $item->name_ar }} | {{  $item->name_en }}
                                    </div> 
                                    @endforeach
                                    <hr class="my-4" />
                                    <h3>الشهادات</h3>
                                    @foreach ($user->certificates as $item)
                                        <p> {{  $item->name }} ||<a href="{{  $item->link }}"> عرض </a></p>
                                   @endforeach
                                  
                                </div>
                            </div>
                        </div>
                    </div>
            <div class="col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-right">
                            <h3 class="mb-0">{{ __('المعلومات الشخصية') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">

                            
                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="pl-lg-4">
                                <div class="form-group">
                                    <label class="form-control-label  pull-right" for="input-name">{{ __('الاسم رباعي') }}</label>
                                    <input  disabled type="text" name="name" id="input-name" class="form-control form-control-alternative" value="{{ $person->first_name.' '.$person->mid_name.' '.$person->last_name.' '.$person->family_name }}">

                                  
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label  pull-right" for="input-email">{{ __('البريد الألكتروني') }}</label>
                                    <input  disabled type="email" name="email" id="input-email" class="form-control form-control-alternative"  value="{{ $user->email }}" >
                                </div>
                                <div class="form-group">
                                        <label class="form-control-label  pull-right" for="input-email">{{ __('تاريخ الميلاد') }}</label>
                                        <input  disabled type="email" name="email" id="input-email" class="form-control form-control-alternative"  value="{{ $person->date_of_birth }}" >
                                </div>

                                <hr class="my-4" />
                                @include('includes.messages')
                                @if($user->id==Auth::user()->id)
                                @if(!$user->hasPromotion())
                                <form method="post" action="{{ route('profile.promotion') }}" autocomplete="off">
                                        @csrf
                                        <div class="row">
                                            <h6>إذا رأيت انك تستحق ترقية يمكنك طلبها, ولكن وحده الاداري يقرر ان يقبلها ام لا</h6>    
                                        </div>                         
                                        <div class="row">
                                           <label class="form-control-label " >طلب ترقية</label> 
                                        </div>
                                        
                                        <div class="row">
   
                                                <select class="form-control {{ $errors->has('description_id') ? ' is-invalid' : '' }}" name="description_id">
                                                    <option value="-1">إختار وصف</option>
                                                    @foreach (Description::all() as $des)
                                                        <option value="{{ $des->id }}" {{ $user->description_id == $des->id ? 'selected' : '' }}>{{ $des->name }}</option>
                                                    @endforeach
                                                </select>      

                                    </div>
                                    <div class="row">
                                        <label class="form-control-label " >ارفق الشهادات لتزيد فرصتك</label>
                                     </div>
                                     <div class="row">
                              
                                         <select class="form-control" name="certificate">
                                                <option value="-1">إختار شهادة</option>
                                                @foreach ($user->certificates as $key => $cer)
                                                    <option value="{{ $cer->id }}" >{{ $cer->name }}</option>
                                                @endforeach
                                            </select>      
   
                                    </div>
                                    <div class="text-center">
                                            <button type="submit" class="btn btn-success mt-4">{{ __('إطلب الان') }}</button>
                                        </div>
                           
                            </form>
                            @endif
                            @endif
                            </div>
                  
                    </div>
                </div>
            </div>
        </div>
        
        {{-- @include('layouts.footers.auth') --}}
    </div>
@endsection
@extends('layouts.main')
@section('styles')

@endsection
@section('content') }}

    <div class="container-fluid mt--7">
      <div class="row">
        
        <div class="col">
          <div class="card shadow">
              @include('includes.messages')
            
                <div>
                    <p class="main-title">إدارة الأمراض | </p>
                    <p class="smale-title">قائمة الأمراض</p>
                </div>
            <div class="row">
              <div>
                <a class="btn btn-success" href="{{ route('diseases.create') }}"> إضافة</span></a>
              </div>
            </div>
            <br>
            <div class="table-responsive">
              <table  id="myTable" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>#رقم</th>
                    <th width="10%">رمز المرض</th>
                    <th width="30%">الاسم العلمي</th>
                    <th width="30%">الاسم الشائع</th>
                    <th width="30%">التخصص</th>
                    <th width="10%">الحالة</th>
                    <th>العمليات</th>
                  </tr>
                </thead>
                <tbody style="text-align: center">
                  @foreach ($diseases as $index => $disease)
                    <tr>
                      <td> {{ $index + 1 }} </td>
                      <td> {{ $disease->code }} </td>
                      <td> {{ $disease->name }} </td>
                      <td> {{ $disease->common_name }} </td>
                      <td> 
                        <div class="badge badge-pill badge-success"> AR  </div>
                        {{ $disease->specialty->name_ar }}
                        <br>
                        <div class="badge badge-pill badge-success"> EN  </div>
                        {{ $disease->specialty->name_en }}
                      
                      </td>

                      <td>
                          @if ($disease->active == 1)
                              <div class="badge badge-success">مفعل</div>
                          @else
                              <div class="badge badge-danger">معطل</div>
                          @endif
                      </td>

                      <td>
                          <a class="btn btn-info  btn-sm" href="{{ route('diseases.show', $disease->id)}}"><span class="fa fa-eye"></span>عرض </a>

                        @if ($disease->active == 1)
                            <a  class="btn btn-danger btn-sm" href="{{route('diseases.changeState', $disease->id)}}"><span class="fa fa-close"></span>تعطيل</a>
                        @else
                            <a href="{{route('diseases.changeState', $disease->id)}}" class="btn btn-success btn-sm"><span class="fa fa-check"></span>تفعيل</a>
                        @endif
                        <a class="btn btn-primary  btn-sm" href="{{ route('diseases.edit', $disease->id)}}"><span class="fa fa-edit"></span>تعديل </a>
                        <a class="btn btn-danger btn-sm remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('diseases.destroy', $disease->id) }}" data-id="{{$disease->id}}" data-target="#custom-width-modal"><span class="fa fa-trash"></span>حذف</a>

                      </td>
                    </tr>  
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
    {{-- <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script> --}}
    {{-- <script src="{{asset('js/jquery.dataTables.min.js')}}"></script> --}}

    {{-- <script>
      $(document).ready( function () {
          $('#myTable').DataTable();
      });
    </script> --}}

@endsection
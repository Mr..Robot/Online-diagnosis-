@extends('layouts.main')

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div>
                    {{-- @include('includes.messages') --}}
                    <p class="main-title">إدارة الأمراض | </p>
                    <p class="smale-title">إضافة مرض</p>
                </div>
                <div class="bord">
                    <form method="POST" action="{{ route('diseases.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12 ">
                                <label class="form-control-label " >رمز المرض</label>
                                <div class="form-group focused {{ $errors->has('code') ? ' has-danger' : '' }}">
                                    <input type="text" name="code" value="{{ old('code') }}" class="form-control {{ $errors->has('code') ? ' is-invalid' : '' }} " placeholder="رمز المرض">
                                    @if ($errors->has('code'))
                                        <span class="is-invalid" role="alert">
                                            <strong>{{ $errors->first('code') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 ">
                                <label class="form-control-label " >الاسم العلمي</label>
                                <div class="form-group focused {{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <input type="text" name="name" value="{{ old('name') }}" class="form-control  {{ $errors->has('name') ? ' is-invalid' : '' }} " placeholder="الاسم العلمي">
                                    @if ($errors->has('name'))
                                        <span class="is-invalid" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 ">
                                <label class="form-control-label " >الاسم الشائع</label>
                                <div class="form-group focused {{ $errors->has('common_name') ? ' has-danger' : '' }}">
                                    <input type="text" name="common_name" value="{{ old('common_name') }}" class="form-control  {{ $errors->has('common_name') ? ' is-invalid' : '' }}" placeholder="الاسم الشائع">
                                    @if ($errors->has('common_name'))
                                        <span class="is-invalid" role="alert">
                                            <strong>{{ $errors->first('common_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 ">
                                <label class="form-control-label " >التخصص</label>
                                <div class="form-group focused {{ $errors->has('specialty_id') ? ' has-danger' : '' }}">
                                    <select class="form-control {{ $errors->has('specialty_id') ? ' is-invalid' : '' }}" name="specialty_id">
                                        <option value="-1">إختار تخصص</option>
                                        @foreach ($specialties as $specialty)
                                            <option value="{{ $specialty->id }}">{{ $specialty->name_ar }} | {{ $specialty->name_en }}</option>
                                        @endforeach
                                    </select>      
                                    @if ($errors->has('specialty_id'))
                                        <span class="is-invalid" role="alert">
                                            <strong>{{ $errors->first('specialty_id') }}</strong>
                                        </span>
                                    @endif                          
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-info"> أعراض المرض</div>
                            </div>
                        </div>
                        <div class="row">
                            @foreach ($symptoms as $key => $symptom)
                                <div class="row my-4">
                                    <div class="col-12">
                                        <div class="custom-control custom-control-alternative custom-checkbox">
                                        <input class="custom-control-input" value="{{ $symptom->id }}" name="symptoms[]" id="symptoms{{ $key }}" type="checkbox">
                                            <label class="custom-control-label" for="symptoms{{ $key }}">
                                                <span class="text-muted">{{ $symptom->name }}</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @if ($errors->has('symptoms'))
                                <span class="is-invalid" role="alert">
                                    <strong>{{ $errors->first('symptoms') }}</strong>
                                </span>
                            @endif                          

                        </div>


                        <div class="row">
                            <div class=" col-md-12 ">
                                <button type="submit" class="btn btn-success">
                                    حفظ <i class="fa fa-check"></i>
                                </button>
                                <a href="{{route('diseases.index')}}" class="btn btn-danger">   إلغاء الأمر <i class="fa fa-close"></i> </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
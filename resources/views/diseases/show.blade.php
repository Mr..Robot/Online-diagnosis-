@extends('layouts.main')

@section('content')
    <div class="container-fluid mt--7">
      <div class="row">
        
        <div class="col">
          <div class="card shadow">
              @include('includes.messages')
            
                <div>
                    <p class="main-title">أعراض المرض </p>
                </div>
            <br>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>#رقم</th>
                    <th width="30%">اسم العرض</th>
                    <th width="30%">الاسم الشائع</th>
                    <th width="20%">الحالة</th>
                  </tr>
                </thead>
                <tbody style="text-align: center">
                  @foreach ($symptoms as $index => $symptom)
                    <tr>
                      <td> {{ $index + 1 }} </td>
                      <td> {{ $symptom->name }} </td>
                      <td> {{ $symptom->common_name }} </td>
                      <td>
                          @if ($symptom->active == 1)
                              <div class="badge badge-success">مفعل</div>
                          @else
                              <div class="badge badge-danger">معطل</div>
                          @endif
                      </td>
                    </tr>  
                  @endforeach

                </tbody>
              </table>
            </div>
            @if ($symptoms->isEmpty())
                <br>
                <div style="text-align: center; color: #f5365c;">لا توجد أعراض لهذا المرض</div>
                <br>
            @endif
          </div>
        </div>
        </div>
        <br>
        <div class="row">
            <div class=" col-md-12 ">
                <a href="{{route('diseases.index')}}" class="btn btn-danger">   رجوع <i class="fa fa-close"></i> </a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection
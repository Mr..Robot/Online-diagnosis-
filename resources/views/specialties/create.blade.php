@extends('layouts.main')

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div>
                    {{-- @include('includes.messages') --}}
                    <p class="main-title">إدارة التخصصات | </p>
                    <p class="smale-title">إضافة تخصص</p>
                </div>
                <div class="bord">
                    <form method="POST" action="{{ route('specialties.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                <label class="form-control-label " >الاسم بالعربية</label>
                                    <div class="form-group focused {{ $errors->has('name_ar') ? ' has-danger' : '' }}">
                                    <input type="text" name="name_ar" value="{{ old('name_ar') }}" class="form-control {{ $errors->has('name_ar') ? ' is-invalid' : '' }} " placeholder="الاسم بالعربية">
                                    @if ($errors->has('name_ar'))
                                        <span class="is-invalid" role="alert">
                                            <strong>{{ $errors->first('name_ar') }}</strong>
                                        </span>
                                    @endif  
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <label class="form-control-label " >الاسم بالانجليزية</label>
                                    <div class="form-group focused {{ $errors->has('name_en') ? ' has-danger' : '' }}">
                                    <input type="text" name="name_en" value="{{ old('name_en') }}"  class="form-control {{ $errors->has('name_en') ? ' is-invalid' : '' }}" placeholder="الاسم بالانجليزية">
                                    @if ($errors->has('name_en'))
                                        <span class="is-invalid" role="alert">
                                            <strong>{{ $errors->first('name_en') }}</strong>
                                        </span>
                                    @endif  
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class=" col-md-12 ">
                                <button type="submit" class="btn btn-success">
                                    حفظ <i class="fa fa-check"></i>
                                </button>
                                <a href="{{route('specialties.index')}}" class="btn btn-danger">   إلغاء الأمر <i class="fa fa-close"></i> </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.main')

@section('content')
    <div class="container-fluid mt--7">
      <div class="row">
        
        <div class="col">
          <div class="card shadow">
              @include('includes.messages')
            
                <div>
                    <p class="main-title">إدارة التخصصات | </p>
                    <p class="smale-title">قائمة التخصصات</p>
                </div>
            <div class="row">
              <div>
                <a class="btn btn-success" href="{{ route('specialties.create') }}"> إضافة</span></a>
              </div>
            </div>
            <br>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>#رقم</th>
                    <th width="30%">التخصص بالعربية</th>
                    <th width="30%">التخصص بالانجليزية</th>
                    <th width="20%">الحالة</th>
                    <th>العمليات</th>
                  </tr>
                </thead>
                <tbody style="text-align: center">
                  @foreach ($specialties as $index => $specialty)
                    <tr>
                      <td> {{ $index + 1 }} </td>
                      <td> {{ $specialty->name_ar }} </td>
                      <td> {{ $specialty->name_en }} </td>

                      <td>
                          @if ($specialty->active == 1)
                              <div class="badge badge-success">مفعل</div>
                          @else
                              <div class="badge badge-danger">معطل</div>
                          @endif
                      </td>

                      <td>
                        <a class="btn btn-info  btn-sm" href="{{ route('specialties.show', $specialty->id)}}"><span class="fa fa-eye"></span>عرض </a>
                        @if ($specialty->active == 1)
                            <a href="{{route('specialties.changeState', $specialty->id)}}" class="btn btn-danger btn-sm"><span class="fa fa-close"></span>تعطيل</a>
                        @else
                            <a href="{{route('specialties.changeState', $specialty->id)}}" class="btn btn-success btn-sm"><span class="fa fa-check"></span>ـفعيل</a>
                        @endif
                        <a class="btn btn-primary  btn-sm" href="{{ route('specialties.edit', $specialty->id)}}"><span class="fa fa-edit"></span>تعديل</a>
                        {{--  <a class="btn btn-danger remove-record btn-sm" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('specialties.destroy', $specialty->id) }}" data-id="{{$specialty->id}}" data-target="#custom-width-modal"><span class="fa fa-trash"></span>حذف</a>  --}}

                      </td>
                    </tr>  
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection
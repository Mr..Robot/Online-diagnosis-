@extends('layouts.main')
@section('styles')
    {{-- <link rel="stylesheet" href="{{asset('css/jquery.dataTables.min.css')}}"> --}}
@endsection
@section('content')

    <div class="container-fluid mt--7">
      <div class="row">
        
        <div class="col">
          <div class="card shadow">
                <div>
                    <p class="main-title">أمراض هذا التخصص</p>
                </div>

            <br>
            <div class="table-responsive">
              <table  id="myTable" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>#رقم</th>
                    <th width="30%">الاسم العلمي</th>
                    <th width="30%">الاسم الشائع</th>
                    <th width="30%">التخصص</th>
                    <th width="20%">الحالة</th>
                  </tr>
                </thead>
                <tbody style="text-align: center">
                  @foreach ($diseases as $index => $disease)
                    <tr>
                      <td> {{ $index + 1 }} </td>
                      <td> {{ $disease->name }} </td>
                      <td> {{ $disease->common_name }} </td>
                      <td> 
                        <div class="badge badge-pill badge-success"> AR  </div>
                        {{ $disease->specialty->name_ar }}
                        <br>
                        <div class="badge badge-pill badge-success"> EN  </div>
                        {{ $disease->specialty->name_en }}
                      
                      </td>

                      <td>
                          @if ($disease->active == 1)
                              <div class="badge badge-success">مفعل</div>
                          @else
                              <div class="badge badge-danger">معطل</div>
                          @endif
                      </td>
                    </tr>  
                  @endforeach

                </tbody>
              </table>
            </div>
            @if ($diseases->isEmpty())
                <br>
                <div style="text-align: center; color: #f5365c;">لا توجد أمراض لهذا التخصص</div>
                <br>
            @endif
          </div>
        </div>
        </div>
        <br>
        <div class="row">
            <div class=" col-md-12 ">
                <a href="{{route('specialties.index')}}" class="btn btn-danger">   رجوع <i class="fa fa-close"></i> </a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
    {{-- <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script> --}}
    {{-- <script src="{{asset('js/jquery.dataTables.min.js')}}"></script> --}}

    {{-- <script>
      $(document).ready( function () {
          $('#myTable').DataTable();
      });
    </script> --}}

@endsection
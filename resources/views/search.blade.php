<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">    
	<!-- Global stylesheets -->
	<link type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet">	
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Design System for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Argon Design System - Free Design System for Bootstrap 4</title>
  <link type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" />
  <link type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link type="text/css" href="{{ asset('website/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
  <link type="text/css" href="{{ asset('website/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link type="text/css" href="{{ asset('website/img/brand/favicon.png')}}" rel="icon" type="image/png">
  <link type="text/css" href="{{ asset('website/css/argon.css?v=1.0.1')}}" rel="stylesheet">
  <link type="text/css" href="{{ asset('assets/css/components.css') }}" rel="stylesheet">
  <link type="text/css" href="{{ asset('template/css/custome.css')}}" rel="stylesheet">
  <link type="text/css" href="{{ asset('css/tags.css') }}" rel="stylesheet" />
  {{-- <link href="{{ asset('css/s.css') }}" rel="stylesheet" /> --}}
  
</head>

<body >
        <section class="section pb-0 section-components">
                <div class="py-5 bg-secondary">
                  <div class="container">
                    <!-- Inputs (alternative) -->
                      <div class="col-lg-12 text-center">
                      <h2 class="display-3">للبحث عن أفضل أخصائي
                      </h2>
                      <h4 class="text-success" style="text-align: center">البحث بالتخصص</h4>
          
          
                      </div>
                        <div class="row">
                          <div class="col-lg-12 ">
                              <form method="POST" action="{{ route('search') }}">
                                @csrf
                                <div id="specialties" class="form-group focused {{ $errors->has('pecialty_id') ? ' has-danger' : '' }}">
                                  <select name="pecialty_id" class="specSelect select form-control {{ $errors->has('pecialty_id') ? ' is-invalid' : '' }}">
                                    <option value="-1">إختار تخصص</option>
                                    @foreach ($specialties as $pecialty)
                                    <option value="{{ $pecialty->id }}">{{ $pecialty->name_ar }} | {{ $pecialty->name_en }}</option>
                                @endforeach
                                </select>
                                    @if ($errors->has('pecialty_id'))
                                        <span class="is-invalid" role="alert">
                                            <strong>{{ $errors->first('pecialty_id') }}</strong>
                                        </span>
                                    @endif                          
                                </div>
                                <h4 class="text-success" style="text-align: center">البحث بالأعراض
          
                                </h4>
                                
                                {{-- <div style="text-align: center">
                                  <label class="custom-toggle">
                                    <input type="checkbox" name="s">
                                    <span class="custom-toggle-slider rounded-circle"></span>
                                  </label>
                                </div> --}}
          
                                <div id="serch">
                                  <div class="col-md-12 ">
                                      <div id = "symptoms" class="row">
                                        <select name="symptomsSelect" id="symptomsSelect" multiple="multiple" class="select form-control">
                                         
                                        </select>
                                        @if ($errors->has('symptoms'))
                                        <span class="is-invalid" role="alert">
                                          <strong>{{ $errors->first('symptoms') }}</strong>
                                        </span>
                                        @endif                          
                                      </div>
                                    </div>
                                  </div>
          
                                  {{-- <div id="serch2">
                                        <div class="col-md-12 ">
                                            <div id = "diseases" class="row">
                                              <select name="diseasesSelect" id="diseasesSelect" multiple="multiple" class="diseasesSelect select form-control">
                                               
                                              </select>
                                              @if ($errors->has('diseases'))
                                              <span class="is-invalid" role="alert">
                                                <strong>{{ $errors->first('diseases') }}</strong>
                                              </span>
                                              @endif                          
                                            </div>
                                          </div>
                                        </div> --}}
            
            
          
          
          
                                <div id="finder" class="btn-wrapper" style="text-align: center">
                                  <button type="submit" class="btn btn-primary mb-3 mb-sm-0">
                                      بحث <i class="fa fa-search"></i>
                                  </button>
                                </div>
                              </form>
                              <br>
                              <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                  <thead class="thead-light">
                                    <tr>
                                      <th width="70%">الطبيب</th>
                                      <th width="30%">|||</th>
                                      <th></th>
                                    </tr>
                                  </thead>
          
                                  <tbody style="text-align: center">
                                    @foreach ($doctors as $index => $doctor)
                                      <tr>
                                        <td> {{ $doctor->person->first_name }} {{ $doctor->person->mid_name }} {{ $doctor->person->last_name }}  {{ $doctor->person->family_name }} </td>
          
                                        <td>
                                          <a class="btn btn-warning " href="{{ route('chats.show', $doctor->id) }}"><span class="fa fa-comment"></span> </a>
                                        </td>
                                      </tr>  
                                    @endforeach
                                  </tbody>
                                </table>
                              </div>         
          
                              <h4 class="text-success" style="text-align: center">أمراض هذه الأعراض</h4>
                              <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                  <thead class="thead-light">
                                    <tr>
                                      <th width="50%">اسم المرض</th>
                                      <th width="50%">الاسم الشائع</th>
          
                                      <th></th>
                                    </tr>
                                  </thead>
          
                                  <tbody style="text-align: center">
                                    @foreach ($diseases as $index => $disease)
                                      <tr>
                                        <td> {{ $disease->name }} </td>
                                        <td> {{ $disease->common_name }} </td>
                                      </tr>  
                                    @endforeach
                                  </tbody>
                                </table>
                              </div>    
          
                          </div>
                        </div>
                  </div>
          
                  <div class="container">
          
                  </div>
                </div>
              </section>
<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
<!-- /core JS files -->
<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>	
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_api.js') }}"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<script src="{{ asset('js/jquery.min.js')}}"></script>
<script src="{{ asset('website/vendor/popper/popper.min.js')}}"></script>
<script src="{{ asset('website/vendor/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{ asset('website/vendor/headroom/headroom.min.js')}}"></script>
<!-- Optional JS -->
<script src="{{ asset('website/vendor/onscreen/onscreen.min.js')}}"></script>
<script src="{{ asset('website/vendor/nouislider/js/nouislider.min.js')}}"></script>
<script src="{{ asset('website/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Argon JS -->
<script src="{{ asset('website/js/argon.js?v=1.0.1')}}"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

{{-- <script src="{{ asset('js/jquery.min.js')}}"></script> --}}
{{-- <script src="{{ asset('js/lodash.min.js')}}"></script> --}}
<script src="{{ asset('js/j.js')}}"></script>

{{-- <script>
 $(document).ready(function() {
          var jsonData = [];
          var fruits = '{{ $symptoms }}'.split(',');
          for(var i=0;i<fruits.length;i++) jsonData.push({id:i,name:fruits[i]});
          var ms1 = $('#ms1').tagSuggest({
              data: jsonData,
              sortOrder: 'name',
              maxDropHeight: 200,
              name: 'ms1'
          });
      });
</script> --}}

<script>
    $(document).ready( function () {
      oTable = $('#mytable').DataTable({
        "bPaginate": false,
        "bInfo" : false,
        "language": {
          "search": ""
        }
      });

      $('#myInputTextField').keyup(function(){
            oTable.search($(this).val()).draw() ;
      });
      $('#finder').hide();
       $('#serch').hide();
      // $('input:checkbox[name=s]').change(function() 
      // {    
      //     if($(this).is(':checked')){
      //         // $('#range-picker').hide();
      //         $('#serch').show();
      //     } else {
      //        $('#serch').hide();
      //     }
           
      // });
    });

</script>

<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/inputs/touchspin.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/pages/form_input_groups.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/form_layouts.js') }}"></script>

<script>
$( ".specSelect" ).change(function() {
  
  if (this.value >= 1){
    $.ajax( 'http://dia.test/secialaties/'+ $(this).val(), {
      type: 'GET',
      dataType: 'json',
      success: function( resp ) {
        $('#serch').show();
        $( "#symptomsSelect" ).empty();
        $.each(resp, function() { 
          $("<option value='" + this.id + "'>" + this.name + "</option>").appendTo("#symptomsSelect");
        });
        
      },error: function( req, status, err ) {
        console.log( 'something went wrong', status, err );
      }
    });
  } else {
    alert("أختار تخصص صحيح");
  }
  
});

$('#symptomsSelect').on('select2:select', function (e) {
//console.log($("#symptomsSelect").val());
//var  data = JSON.stringify( {  });
//var  data['data'] = $("#symptomsSelect").val(); 
//alert(data);
$('#finder').show();

// var dataVars = {};
// dataVars['ids'] = $("#symptomsSelect").val();


// console.log( dataVars );
// console.log();
//     $.ajax( 'http://dia.test/api/DbySymptoms', {
//       type: 'POST',
//       data: dataVars,
//       dataType: 'json',
//       success: function( resp ) {
//         console.log( resp );
//       }
//         // $('#serch2').show();
//         // $( "#diseasesSelect" ).empty();
//         // $.each(resp, function() { 
//         //   $("<option value='" + this.id + "'>" + this.name + "</option>").appendTo("#diseasesSelect");
//         // });
        
//       ,error: function( req, status, err ) {
//         console.log( 'something went wrong', status, err );
//       }
//     });

});

    // $(document).on('submit','form', function(e){
    //     e.preventDefault();

    //     $.ajax({
    //         url: $(this).attr('action'),
    //         type: $(this).attr('method'),
    //         data: $(this).serialize(),
    //         success(d){
    //             $("#panel-body").html(d);
    //         },
    //         error(e){
    //             console.log(e);
    //         }
    //     });
    // });
</script>
</body>
</html>
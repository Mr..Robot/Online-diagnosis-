    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="{{ route('home') }}">الرئيسية</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="{{ asset('template/img/logo.png') }}">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  @if (Auth::user())
                    <span class="mb-0 text-sm  font-weight-bold">{{ Auth::user()->name }}</span>   
                  @endif
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-left">
              <div class=" dropdown-header noti-title">
              </div>
             
              @if (!Auth::user())
                <a href="{{ route('login') }}" class="dropdown-item">
                  <i class="ni ni-settings-gear-65"></i>
                  <span>تسجيل الدخول</span>
                </a>
                {{-- <a href="{{ route('register') }}" class="dropdown-item">
                  <i class="ni ni-settings-gear-65"></i>
                  <span>إنشاء حساب</span>
                </a> --}}
              @else 
                {{-- <a href="{{ route('chats.index') }}" class="dropdown-item">
                  <i class="fa fa-comments"></i>
                  <span>المحادثات</span>
                </a> --}}

                <a href="/profile" class="dropdown-item">
                  <i class="ni ni-settings-gear-65"></i>
                  <span>الملف الشخصي</span>
                </a>

                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                  <i class="ni ni-user-run"></i>
                  <span>تسجيل الخروج</span>
                </a>

              @endif

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
           
            </div>
          </li>
        </ul>
      </div>
    </nav>

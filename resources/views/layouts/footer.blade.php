  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{asset('template/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{asset('template/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <!-- Optional JS -->
  <script src="{{asset('template/vendor/chart.js/dist/Chart.min.js')}}"></script>
  <script src="{{asset('template/vendor/chart.js/dist/Chart.extension.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{asset('template/js/argon.js?v=1.0.0')}}"></script>
  <script src="{{asset('js/multiselect.js')}}"></script>
  @yield('scripts')
</body>
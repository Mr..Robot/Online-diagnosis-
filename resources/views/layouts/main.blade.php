@include('layouts.header')
@include('layouts.sidbar')

  <!-- Main content -->
  <div class="main-content">
    @include('layouts.navbar')
    <!-- Header -->
    @include('layouts.page-header')
    <!-- Page content -->
    @yield('content')
  </div>


@include('layouts.footer')
@yield('modals')

<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="./index.html">
        <img src="{{asset('template/img/brand/blue.png')}}" class="navbar-brand-img" alt="...">
      </a>
      <!-- User -->
      <ul class="nav align-items-center d-md-none">
        <li class="nav-item dropdown">
          <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="ni ni-bell-55"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="./assets/img/theme/team-1-800x800.jpg">
              </span>
            </div>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <div class=" dropdown-header noti-title">
              <h6 class="text-overflow m-0">Welcome!</h6>
            </div>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-single-02"></i>
              <span>My profile</span>
            </a>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-settings-gear-65"></i>
              <span>Settings</span>
            </a>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-calendar-grid-58"></i>
              <span>Activity</span>
            </a>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-support-16"></i>
              <span>Support</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#!" class="dropdown-item">
              <i class="ni ni-user-run"></i>
              <span>Logout</span>
            </a>
          </div>
        </li>
      </ul>
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="./index.html">
                <img src="{{asset('template/img/brand/blue.png')}}">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!-- Form -->
        <form class="mt-4 mb-3 d-md-none">
          <div class="input-group input-group-rounded input-group-merge">
            <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <span class="fa fa-search"></span>
              </div>
            </div>
          </div>
        </form>

        <ul class="navbar-nav">
            <h6 class="navbar-heading text-muted" style="text-align: right">السجل الصحي  <i class="single-copy-04"></i> </h6>

            <li class="nav-item">
                <a class="nav-link" href="{{route('specialties.index')}}">
                <i class="ni ni-planet text-primary"></i> التخصصات
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('symptoms.index')}}">
                    <i class="ni ni-planet text-primary"></i> الأعراض
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('diseases.index')}}">
                    <i class="ni ni-planet text-primary"></i> الأمراض
                </a>
            </li>

            @if (Auth::user()->type_id == 2)
                <h6 class="navbar-heading text-muted" style="text-align: right">الأطباء</h6>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('doctor_specialties.index')}}">
                        <i class="ni ni-planet text-primary"></i> أختصاصات الطبيب
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('doctor_work.index')}}">
                        <i class="ni ni-planet text-primary"></i> أعمال الطبيب
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('doctor_certificates.index')}}">
                        <i class="ni ni-planet text-primary"></i> شهادات الطبيب
                    </a>
                </li>
            @endif
            @if (Auth::user()->type_id == 1)
                <h6 class="navbar-heading text-muted" style="text-align: right">الإعدادات</h6>
                <li class="nav-item">
                  <a class="nav-link" href="{{route('nationalities.index')}}">
                      <i class="ni ni-planet text-primary"></i> الجنسيات
                  </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('doctors.index')}}">
                        <i class="ni ni-planet text-primary"></i> طلبات التسجيل
                    </a>
                </li>
            @endif

        </ul>

        </div>
    </div>
</nav>

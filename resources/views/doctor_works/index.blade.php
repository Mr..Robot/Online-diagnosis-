@extends('layouts.main')

@section('content')

    <div class="container-fluid mt--7">
      <div class="row">
        
            <div class="col">
                    <div class="card shadow">
                        @include('includes.messages')
                      
                          <div>
                              <p class="main-title">إدارة الاعمال | </p>
                              <p class="smale-title">قائمة الاعمال</p>
                          </div>
                      <div class="row">
                        <div>
                          <a class="btn btn-success" href="{{ route('doctor_work.create') }}"> إضافة</span></a>
                        </div>
                      </div>
                      <br>
                      <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                          <thead class="thead-light">
                            <tr>
                              <th>#رقم</th>
                              <th width="30%">اسم العمل</th>
                              <th>العمليات</th>
                            </tr>
                          </thead>
                          <tbody style="text-align: center">
                            @foreach (Auth::user()->works as $index => $work)
                              <tr>
                                <td> {{ $index + 1 }} </td>
                                <td> {{ $work->name }} </td>
                                <td>
                                  <a class="btn btn-primary  btn-sm" href="{{ route('doctor_work.edit', $work->id)}}"><span class="fa fa-edit"></span>تعديل </a>
                                  <a class="btn btn-danger btn-sm remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('doctor_work.destroy', $work->id) }}" data-id="{{$work->id}}" data-target="#custom-width-modal"><span class="fa fa-trash"></span>حذف</a>
                                </td>
                              </tr>  
                            @endforeach
          
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
    </div>
</div>
@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
    {{-- <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script> --}}
    {{-- <script src="{{asset('js/jquery.dataTables.min.js')}}"></script> --}}

    {{-- <script>
      $(document).ready( function () {
          $('#myTable').DataTable();
      });
    </script> --}}

@endsection
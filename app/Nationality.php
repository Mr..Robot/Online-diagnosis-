<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes; // <-- This is required

use Illuminate\Database\Eloquent\Model;

class Nationality extends Model
{
    use SoftDeletes; // <-
    protected $dates = ['deleted_at'];
    //
}

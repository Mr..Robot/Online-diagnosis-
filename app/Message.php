<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes; // <-- This is required

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use SoftDeletes; // <-
    protected $dates = ['deleted_at'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

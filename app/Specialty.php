<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes; // <-- This is required

use Illuminate\Database\Eloquent\Model;

class Specialty extends Model
{
    use SoftDeletes; // <-
    protected $dates = ['deleted_at'];
    public function diseases()
    {
        return $this->hasMany('App\Disease');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'doctor_specialties');
    }
}

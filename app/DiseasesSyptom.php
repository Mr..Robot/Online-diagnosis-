<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes; // <-- This is required

use Illuminate\Database\Eloquent\Model;

class DiseasesSyptom extends Model
{
    use SoftDeletes; // <-
    protected $dates = ['deleted_at'];
    public function symptom()
    {
        return $this->belongsTo(Symptom::class);
    }

}

<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes; // <-- This is required

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
    use SoftDeletes; // <-
    protected $dates = ['deleted_at'];
    public function specialty()
    {
        return $this->belongsTo('App\Specialty');
    }

    public function symptoms()
    {
        return $this->belongsToMany(Symptom::class, 'diseases_syptoms');
    }
}

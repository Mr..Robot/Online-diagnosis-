<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes; // <-- This is required

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use SoftDeletes; 
    protected $dates = ['deleted_at'];
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }
    public function recived()
    {
        return $this->belongsTo(User::class, 'received_id', 'id');
    }
}

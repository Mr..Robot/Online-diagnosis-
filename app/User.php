<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes; // <-- This is required

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $dates = ['deleted_at'];
    public function specialties()
    {
        return $this->belongsToMany(Specialty::class, 'doctor_specialties');
    }

    public function chats()
    {
        return $this->hasMany(Chat::class, 'sender_id', 'id');
    }

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function doctor()
    {
        return $this->belongsTo(Doctor::class, 'id', 'user_id');
    }
    public function description()
    {
        return $this->belongsTo(Description::class);
    }
    public function work()
    {
        return $this->hasMany(Work::class);
    }

    public function works()
    {
        return $this->hasMany(Work::class);
    }

    public function certificates()
    {
        return $this->hasMany(Certificate::class);
    }
    public function promotions()
    {
        return $this->hasMany(Promotion::class);
    }

    public function hasPromotion()
    {
        return $this->promotions->isNotEmpty();
    }
    public function isVeriFied()
    {
        return !is_null($this->email_verified_at);
    }

  
}

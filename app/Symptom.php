<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes; // <-- This is required

use Illuminate\Database\Eloquent\Model;

class Symptom extends Model
{
    use SoftDeletes; // <-
    protected $dates = ['deleted_at'];
    public function disease()
    {
        return $this->belongsToMany(Disease::class, 'diseases_syptoms');
    }

    public function diseases()
    {
        return $this->belongsToMany(Disease::class, 'diseases_syptoms');
    }
}

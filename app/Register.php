<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes; // <-- This is required

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    use SoftDeletes; // <-
    protected $dates = ['deleted_at'];
    //
}

<?php

namespace App\Http\Controllers;

use App\User;
use App\Doctor;
use App\Person;
use App\Register;
use App\Nationality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nationalities = Nationality::all();
        $data = [
            'nationalities' => $nationalities
        ];

        if (Auth::guest()){
            return view ('rigister.register')->with($data);
        } else {
            return view ('blank');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nationalities = Nationality::all();
        $data = [
            'nationalities' => $nationalities
        ];
        return view ('rigister.register')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'first_name' => 'required|string|min:2|max:50',
            'mid_name' => 'required|string|min:2|max:50',
            'last_name' => 'required|string|min:2|max:50',
            'family_name' => 'required|string|min:2|max:50',
            'date_of_birth' => 'required|date',
            'gender' => 'required|integer|min:0',
            'nationality_id' => 'required|integer|exists:nationalities,id',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],

        ];

        $messages = [
            'gender.required' => 'الرجاء أختيار الجنس',
            'gender.min' => 'الرجاء أختيار الجنس'
        ];

        $this->validate($request, $rules,$messages);

        $person = new Person;
        $person->first_name = $request->first_name;
        $person->mid_name = $request->mid_name;
        $person->last_name = $request->last_name;
        $person->family_name = $request->family_name;
        $person->date_of_birth = $request->date_of_birth;
        $person->gender = $request->gender;
        $person->nationality_id = $request->nationality_id;

        $person->save();

        $user = new User;
        $user->email = $request->email;
        $user->password = bcrypt($request->get('password'));
        $user->name = $request->first_name;
        $user->person_id = $person->id;
        $user->type_id = 3;
        $user->active = 1;
        
        $user->save();

        return redirect()->route('login');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Register  $register
     * @return \Illuminate\Http\Response
     */
    public function show(Register $register)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Register  $register
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_doctor()
    {
        $nationalities = Nationality::all();
        $data = [
            'nationalities' => $nationalities
        ];
        return view ('rigister.register-doctor')->with($data);
    }

    public function store_doctor(Request $request)
    {
        // if ($request->hasFile('cover_image')){
        //     return 'file';
        // }
        $rules = [
            'first_name' => 'required|string|min:2|max:50|alpha',
            'mid_name' => 'required|string|min:2|max:50|alpha',
            'last_name' => 'required|string|min:2|max:50|alpha',
            'family_name' => 'required|string|min:2|max:50|alpha',
            'date_of_birth' => 'required|date',
            'gender' => 'required|integer|min:0',
            'nationality_id' => 'required|integer|exists:nationalities,id',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'cover_image' => 'required|max:4096',
            'dgree' => 'required',
            'ref_no' => 'required|unique:doctors,ref_no',
        ];

        // // return $request->cover_image;
        // if ($request->hasFile('cover_image')){
            

        if ($request->has('cover_image')){ 
            $path = $request->file('cover_image')->store('public/cover_image/');//->filename();
            $fileNameToStore = 'storage/certs/'.basename($path);//->name;
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $messages = [
            'gender.required' => 'الرجاء أختيار الجنس',
            'gender.min' => 'الرجاء أختيار الجنس'
        ];

        $this->validate($request, $rules,$messages);

        $person = new Person;
        $person->first_name = $request->first_name;
        $person->mid_name = $request->mid_name;
        $person->last_name = $request->last_name;
        $person->family_name = $request->family_name;
        $person->date_of_birth = $request->date_of_birth;
        $person->gender = $request->gender;
        $person->nationality_id = $request->nationality_id;

        $person->save();

        $user = new User;
        $user->email = $request->email;
        $user->password = bcrypt($request->get('password'));
        $user->name = $request->first_name;
        $user->type_id = 2;
        $user->person_id = $person->id;
        $user->active = 0;
                
        $user->cover_image  =  $fileNameToStore;

        $user->save();

        $doctor = New Doctor;
        $doctor->dgree = $request->dgree;
        $doctor->ref_no = $request->ref_no;
        $doctor->user_id = $user->id;
        $doctor->save();

        return redirect()->route('login');
    }

    public function edit(Register $register)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Register  $register
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Register $register)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Register  $register
     * @return \Illuminate\Http\Response
     */
    public function destroy(Register $register)
    {
        //
    }
}

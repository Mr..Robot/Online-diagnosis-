<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Session\Session;

class ChatController extends Controller
{
/** HomeController.php
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chats = Chat::where('sender_id', Auth::user()->id)->orWhere('received_id', Auth::user()->id)->get();
        return view('chat.index')->with(['chats' => $chats]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = New Message();
        $message->chat_id = $request->chat_id;
        $message->text = $request->message;
        $message->user_id = Auth::user()->id;
        $message->date = Carbon::now();
        $message->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $chat = Chat::where('sender_id', Auth::user()->id)->where('received_id', $id)->get();
        if ($chat->isEmpty()){
            $c = new Chat();
            $c->sender_id = Auth::user()->id;
            $c->received_id = $id;
            $c->save();
        }
        $chats = Chat::where('sender_id', Auth::user()->id)->orWhere('received_id', Auth::user()->id)->get();
        return view('chat.index')->with(['chats' => $chats]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $messages  = Message::where('chat_id' , $id)->get();

        $data = [
            'messages' => $messages,
            'id' => $id
        ];
        return view('chat.create')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chat $chat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chat $chat)
    {
        //
    }
}

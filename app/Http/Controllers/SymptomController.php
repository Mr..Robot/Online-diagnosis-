<?php

namespace App\Http\Controllers;

use App\Symptom;
use Illuminate\Http\Request;

class SymptomController extends Controller
{
    /** HomeController.php
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $symptoms = Symptom::all();

        $data = [
            'symptoms' => $symptoms
        ];

        return View('symptoms.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('symptoms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|min:1|unique:symptoms,name|alpha',
            'common_name' => 'required|string|min:1|unique:symptoms,common_name|alpha',
        ];

        $messages = [
        ];

        $this->validate($request, $rules,$messages);

        $symptom = new Symptom;
        $symptom->name = $request->name;
        $symptom->common_name = $request->common_name;
        $symptom->save();

        return redirect()->route('symptoms.index')->with('success', 'تم إضافة العرض إلى قاعدة البيانات بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Symptom  $symptom
     * @return \Illuminate\Http\Response
     */
    public function show(Symptom $symptom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Symptom  $symptom
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $symptom = Symptom::find($id);
        return view('symptoms.edit')->with(['symptom' => $symptom]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Symptom  $symptom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|string|min:1|unique:symptoms,name|alpha' . $id,
            'common_name' => 'required|string|min:1|unique:symptoms,common_name|alpha' . $id,
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules,$messages);

        $symptom = Symptom::findOrFail($id);

        $symptom->name = $request->name;
        $symptom->common_name = $request->common_name;
        $symptom->update();

        return redirect()->route('symptoms.index')->with('success', 'تم تعديل العرض في قاعدة البيانات بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Symptom  $symptom
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$symptom = 
        Symptom::find($id)->delete();
       // $symptom->delete();

        return redirect()->route('symptoms.index')->with('success','تمت عملية حذف العرض من قاعدة البيانات بنجاح');
    }
    
    public function changeState($id)
    {
        $symptom = Symptom::findOrFail($id);
        $symptom->active = !$symptom->active;
        $symptom->update();
        
        $msg = '';
        if ($symptom->active == 1) {
            $msg = 'تمت عملية تفعيل العرض في قاعدة البيانات بنجاح';
        } else {
            $msg = 'تمت عملية تعطيل العرض في قاعدة البيانات بنجاح';
        }
        return redirect()->route('symptoms.index')->with('success', $msg);
    }
}

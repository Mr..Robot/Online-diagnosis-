<?php

namespace App\Http\Controllers;

use App\Nationality;
use Illuminate\View\View;
use Illuminate\Http\Request;

class NationalityController extends Controller
{/** HomeController.php
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nationalitits = Nationality::all();

        $data = [
            'nationalitits' => $nationalitits
        ];

        return View('nationalities.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nationalities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nationality_name' => 'required|string|min:1|unique:nationalities,nationality_name|alpha',
        ];

        $messages = [
        ];

        $this->validate($request, $rules,$messages);

        $nationality = new Nationality;
        $nationality->nationality_name = $request->nationality_name;
        $nationality->save();

        return redirect()->route('nationalities.index')->with('success', 'تم إضافة الجنسية إلى قاعدة البيانات بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nationality  $nationality
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nationality  $nationality
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    
        $nationality = Nationality::find($id);
        return view('nationalities.edit')->with(['nationality' => $nationality]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nationality  $nationality
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $rules = [
            'nationality_name' => 'required|string|min:1|unique:nationalities,nationality_name,',
        ];

        $messages = [
        ];

        $this->validate($request, $rules,$messages);

        $nationality = Nationality::findOrFail($id);

        $nationality->nationality_name = $request->nationality_name;
        $nationality->update();

        return redirect()->route('nationalities.index')->with('success', 'تم تعديل الجنسية إلى قاعدة البيانات بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nationality  $nationality
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Nationality::find($id)->delete();
        // $symptom->delete();
 
         return redirect()->route('nationalities.index')->with('success','تمت عملية حذف الجنسية من قاعدة البيانات بنجاح');
        //
    }
}

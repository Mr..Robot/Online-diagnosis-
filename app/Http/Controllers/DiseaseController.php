<?php

namespace App\Http\Controllers;

use App\Disease;
use App\Symptom;
use App\Specialty;
use Illuminate\Http\Request;

class DiseaseController extends Controller
{/** HomeController.php
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diseases = Disease::all();

        $data = [
            'diseases' => $diseases
        ];

        return View('diseases.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $symptoms = Symptom::all();

        $specialties = Specialty::all();

        $data = [
            'specialties' => $specialties,
            'symptoms' => $symptoms
        ];
        return view('diseases.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'code' => 'required|string|min:1|max:100|unique:diseases,code',
            'name' => 'required|string|min:1|max:100|unique:diseases,name|alpha',
            'common_name' => 'required|string|min:1|max:100|unique:diseases,common_name|alpha',
            'specialty_id' => 'required|integer|exists:specialties,id',
            'symptoms' => 'required',
        ];

        $messages = [
        ];

        $this->validate($request, $rules,$messages);

        $disease = new Disease;
        $disease->code = $request->code;
        $disease->name = $request->name;
        $disease->common_name = $request->common_name;
        $disease->specialty_id = $request->specialty_id;
        $disease->save();

        $disease->symptoms()->attach($request->symptoms);

        return redirect()->route('diseases.index')->with('success', 'تم إضافة المرض إلى قاعدة البيانات بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Disease  $disease
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $disease =  Disease::findOrFail($id);

        $symptoms = $disease->symptoms;
        $data = [
            'symptoms' => $symptoms
        ];
        return view('diseases.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Disease  $disease
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $symptoms = Symptom::all();
        $specialties = Specialty::all();
        $disease = Disease::find($id);
        $symptoms_disease = $disease->symptoms()->pluck('id')->toArray();
         

        $data = [
            'specialties' => $specialties,
            'disease' => $disease,
            'symptoms' => $symptoms,
            'symptoms_disease' => $symptoms_disease,
        ];


        return view('diseases.edit')->with($data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Disease  $disease
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'code' => 'required|string|min:1|max:100|unique:diseases,code, ',
            'name' => 'required|string|min:1|max:100|unique:diseases,name|alpha ',
            'common_name' => 'required|string|min:1|max:100|unique:diseases,common_name|alpha ',
            'specialty_id' => 'required|integer|exists:specialties,id',
            'symptoms' => 'required',
        ];

        $messages = [
        ];

        $this->validate($request, $rules,$messages);

        $disease = Disease::findOrFail($id);

        $disease->code = $request->code;
        $disease->name = $request->name;
        $disease->common_name = $request->common_name;
        $disease->specialty_id = $request->specialty_id;
        $disease->update();

        $disease->symptoms()->detach();
        $disease->symptoms()->attach($request->symptoms);

        return redirect()->route('diseases.index')->with('success', 'تم تعديل المرض إلى قاعدة البيانات بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Disease  $disease
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $disease = Disease::find($id);
        $disease->symptoms()->detach();
        $disease->delete();

        return redirect()->route('diseases.index')->with('success','تمت عملية حذف المرض من قاعدة البيانات بنجاح');
    }

        
    public function changeState($id)
    {
        $disease = Disease::findOrFail($id);
        $disease->active = !$disease->active;
        $disease->update();
        
        $msg = '';
        if ($disease->active == 1) {
            $msg = 'تمت عملية تفعيل المرض في قاعدة البيانات بنجاح';
        } else {
            $msg = 'تمت عملية تعطيل المرض في قاعدة البيانات بنجاح';
        }
        return redirect()->route('diseases.index')->with('success', $msg);
    }

}

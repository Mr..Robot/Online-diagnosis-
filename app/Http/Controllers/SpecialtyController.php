<?php

namespace App\Http\Controllers;

use App\Disease;
use App\Specialty;
use Illuminate\Http\Request;

class SpecialtyController extends Controller
{
    /** HomeController.php
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specialties = Specialty::all();

        $data = [
            'specialties' => $specialties
        ];

        return View('specialties.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('specialties.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name_ar' => 'required|string|min:1|unique:specialties,name_ar|alpha',
            'name_en' => 'required|string|min:1|unique:specialties,name_en|alpha',
        ];

        $messages = [
        ];

        $this->validate($request, $rules,$messages);

        $specialty = new Specialty;
        $specialty->name_ar = $request->name_ar;
        $specialty->name_en = $request->name_en;
        $specialty->save();

        return redirect()->route('specialties.index')->with('success', 'تم إضافة التخصص إلى قاعدة البيانات بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Specialty  $specialty
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $specialty =  Specialty::findOrFail($id);

        $diseases =  $specialty->diseases;
        $data = [
            'diseases' => $diseases
        ];
        return view('specialties.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Specialty  $specialty
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $specialty = Specialty::find($id);
        return view('specialties.edit')->with(['specialty' => $specialty]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Specialty  $specialty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name_ar' => 'required|string|min:1|unique:specialties,name_ar|alpha,' . $id,
            'name_en' => 'required|string|min:1|unique:specialties,name_en|alpha,' . $id,
        ];

        $messages = [
        ];

        $this->validate($request, $rules,$messages);

        $specialty = Specialty::findOrFail($id);

        $specialty->name_ar = $request->name_ar;
        $specialty->name_en = $request->name_en;
        $specialty->update();

        return redirect()->route('specialties.index')->with('success', 'تم تعديل التخصص في قاعدة البيانات بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Specialty  $specialty
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$specialty = 
        Specialty::find($id)->delete();
     //   $specialty->delete();

        return redirect()->route('specialties.index')->with('success','تمت عملية حذف التخصص من قاعدة البيانات بنجاح');
    }

    
    public function changeState($id)
    {
        $specialty = Specialty::findOrFail($id);
        $specialty->active = !$specialty->active;
        $specialty->update();
        
        $msg = '';
        if ($specialty->active == 1) {
            $msg = 'تمت عملية تفعيل التخصص في قاعدة البيانات بنجاح';
        } else {
            $msg = 'تمت عملية تعطيل التخصص في قاعدة البيانات بنجاح';
        }
        return redirect()->route('specialties.index')->with('success', $msg);
    }

}

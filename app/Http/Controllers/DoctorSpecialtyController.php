<?php

namespace App\Http\Controllers;

use App\Specialty;
use App\DoctorSpecialty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DoctorSpecialtyController extends Controller
{/** HomeController.php
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctor_specialties = Auth::user()->specialties()->pluck('specialty_id')->toArray();

        $specialties = Specialty::all();
 
        $data = [
            'doctor_specialties' => $doctor_specialties,
            'specialties' => $specialties
        ];

        return View('doctor_specialties.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'specialties' => 'required',
        ];

        $messages = [
        ];

        $this->validate($request, $rules,$messages);

        $doctor = Auth::user();

        $doctor->specialties()->detach();
        $doctor->specialties()->attach($request->specialties);

        return redirect()->route('doctor_specialties.index')->with('success', 'تم حفظ تخصصات الطبيب في قاعدة البيانات بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Disease  $disease
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Disease  $disease
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Disease  $disease
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Disease  $disease
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

        
    public function changeState($id)
    {
    }

}

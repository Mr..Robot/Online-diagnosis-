<?php
namespace App\Http\Controllers;
    
use App\Specialty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Work;

class DoctorWorkController extends Controller
{/** HomeController.php
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $doctor_works = Auth::user()->works()->pluck('id')->toArray();
    
            $works = Specialty::all();
     
            $data = [
                'doctor_works' => $doctor_works,
                'works' => $works
            ];
    
            return View('doctor_works.index')->with($data);
        }
    
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('doctor_works.create');
        }
    
        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
          //  return $request->name;
            $rules = [
                'name' => 'required',
            ];
    
            $messages = [
            ];
    
            $this->validate($request, $rules,$messages);
    
            $user = Auth::user();
            $work = New Work();
            $work->user_id = $user->id;
            $work->name = $request->name;
            $work->save();
        
            return redirect()->route('doctor_work.index')->with('success', 'تم حفظ تخصصات الطبيب في قاعدة البيانات بنجاح');
        }
    
        /**
         * Display the specified resource.
         *
         * @param  \App\Disease  $disease
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {        

        }
    
        /**
         * Show the form for editing the specified resource.
         *
         * @param  \App\Disease  $disease
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $work = Work::find($id);
            if(Auth::User()->works->contains($work)){
                return view('doctor_works.edit')->with(['work' => $work]);
            }
            return view('doctor_works.index');//->with(['work' => $work]);
    
        }
    
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \App\Disease  $disease
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $rules = [
                'name' => 'required|string|min:1' . $id,
            ];
    
            $messages = [
            ];
            
            $this->validate($request, $rules,$messages);
    
            $work = Work::findOrFail($id);
            $work->name = $request->name;
            $work->update();
    
            return redirect()->route('doctor_work.index')->with('success', 'تم تعديل العمل في قاعدة البيانات بنجاح');
        }
    
        /**
         * Remove the specified resource from storage.
         *
         * @param  \App\Disease  $disease
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            
        //$symptom = 
        Work::find($id)->delete();
        // $symptom->delete();
 
         return redirect()->route('doctor_work.index')->with('success','تمت عملية حذف العمل من قاعدة البيانات بنجاح');
    
        }
    }
    
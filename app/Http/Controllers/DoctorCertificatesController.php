<?php

namespace App\Http\Controllers;

use App\Specialty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Certificate;
use Illuminate\Support\Facades\Storage;
class DoctorCertificatesController extends Controller
{/** HomeController.php
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
           /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $doctor_certificates = Auth::user()->certificates()->pluck('id')->toArray();
    
            $certificates = Specialty::all();
     
            $data = [
                'doctor_certificates' => $doctor_certificates,
                'certificates' => $certificates
            ];
    
            return View('doctor_certificates.index')->with($data);
        }
    
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('doctor_certificates.create');
        }
    
        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
           // dd($request);
          //  return $request->name;
            $rules = [
                'name' => 'required',
                'cover_image' => 'required|max:4096',
            ];
    
            $messages = [
            ];
            $this->validate($request, $rules,$messages);
           // return $request->cover_image; 
           $user = Auth::user();
           $certificate = New Certificate();
           $certificate->user_id = $user->id;
           $certificate->name = $request->name;
           
           if ($request->has('cover_image')){ 
            $path = $request->file('cover_image')->store('public/certs');//->filename();
            $certificate->link = 'storage/certs/'.basename($path);//->name;
        } else {
            $fileNameToStore = 'noimage.jpg';
        }
            $certificate->save();
    
         
            return redirect()->route('doctor_certificates.index')->with('success', 'تم حفظ شهادة الطبيب في قاعدة البيانات بنجاح');
        }
    
        /**
         * Display the specified resource.
         *
         * @param  \App\Disease  $disease
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {        

        }
    
        /**
         * Show the form for editing the specified resource.
         *
         * @param  \App\Disease  $disease
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $certificate = Certificate::find($id);
            if(Auth::User()->certificates->contains($certificate)){
                return view('doctor_certificates.edit')->with(['certificate' => $certificate]);
            }
            return view('doctor_certificates.index');//->with(['certificate' => $certificate]);
    
        }
    
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \App\Disease  $disease
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $rules = [
                'name' => 'required|string|min:1',
            ];
    
            $messages = [
            ];
            
            $this->validate($request, $rules,$messages);
    
            $certificate = Certificate::findOrFail($id);
            $certificate->name = $request->name;
            $certificate->update();
    
            return redirect()->route('doctor_certificates.index')->with('success', 'تم تعديل  اسم الشهادة في قاعدة البيانات بنجاح');
        }
    
        /**
         * Remove the specified resource from storage.
         *
         * @param  \App\Disease  $disease
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            
        //$symptom = 
        Certificate::find($id)->delete();
        // $symptom->delete();
 
         return redirect()->route('doctor_certificates.index')->with('success','تمت عملية حذف الشهادة من قاعدة البيانات بنجاح');
    
        }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Promotion;
use App\User;

class ProfileController extends Controller
{/** HomeController.php
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    public function show(Request $request)
    {
        $user = Auth::user();
        $person = $user->person;
        $data = [
            'user' => $user,
            'person' => $person
        ];

        return View('profile.show')->with($data);

    }

    public function find($id)
    {
        $user = User::find($id);
        $person = $user->person;
        $data = [
            'user' => $user,
            'person' => $person
        ];

        return View('profile.show')->with($data);

    }


    public function edit(Request $request)
    {
        return Auth::user();
    }
    public function update(Request $request)
    {
        return Auth::user();
    }
    public function promotion(Request $request)
    {
        $user = Auth::user();
        $pro = New Promotion();
        $pro->user_id = $user->id;
        $pro->description_id = $request->description_id;
        $pro->certificate_id = $request->certificate;
        $pro->save();
        return $pro;
    }

}

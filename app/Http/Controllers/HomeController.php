<?php

namespace App\Http\Controllers;

use App\User;
use App\Person;
use App\Disease;
use App\Symptom;
use App\Specialty;
use App\DoctorSpecialty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      
      //  if (Auth::guest()){
            $specialties = Specialty::all();
            // $symptoms  = Symptom::all()->pluck('common_name')->toArray();
            $symptoms  = Symptom::all();
            // $symptoms = implode(",",$symptoms);
            $doctors = [];
            $data = [
                'doctors' => $doctors,
                'specialties' => $specialties,
                'symptoms' => $symptoms ,
                 'diseases' => []
            ];
            return view('dashboard')->with( $data);
      //  } else {
        //    return view ('blank');
       // }

    }


    public function dash()
    {
      
    
           return view ('blank');
   
    }





    public function view (Request $request){
        $specialties = Specialty::all();
        // $symptoms  = Symptom::all()->pluck('common_name')->toArray();
        $symptoms  = Symptom::all();
        // $symptoms = implode(",",$symptoms);
        $doctors = [];
        $data = [
            'doctors' => $doctors,
            'specialties' => $specialties,
            'symptoms' => $symptoms ,
             'diseases' => []
        ];
        return view('search')->with( $data);
 

    }


    public function search (Request $request){


        if ($request->s == 'on'){
            // return 'a';
            $diseases = Disease::whereHas('symptoms', function ($q) use($request) {
                $q->whereIn('id', $request->symptoms);
            })->get();

            // return $diseases; //->pluck('specialty_id')->toArray()

            // return $disease;
            // return $s = DoctorSpecialty::whereIn('specialty_id', $disease)->get()->pluck('specialty_id')->toArray();
            
            // $doctors = User::whereHas('specialties', function($q) use ($request) {
            //     $q->where('specialty_id', $request->pecialty_id);
            // })->get();

            $doctors = User::whereHas('specialties', function($q) use ($diseases) {
                $q->whereIn('specialty_id',$diseases);
            })->get();
            // return $doctors;
            
            $symptoms  = Symptom::all();
            $specialties = Specialty::all();
            $data = [
                'doctors' => $doctors,
                'specialties' => $specialties,
                'symptoms' => $symptoms,
                'diseases' => $diseases
            ];

            return view('dashboard')->with($data);
        } else {

            $doctors = User::whereHas('specialties', function($q) use ($request) {
                $q->where('specialty_id', $request->pecialty_id);
            })->get();

            $symptoms  = Symptom::all();
            $specialties = Specialty::all();
            $data = [
                'doctors' => $doctors,
                'specialties' => $specialties,
                'symptoms' => $symptoms,
                'diseases' => []
            ];

            return view('dashboard')->with($data);
        }

    }
}

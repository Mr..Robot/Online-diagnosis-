-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2019 at 05:10 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onlinediagnosis`
--

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `received_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `sender_id`, `received_id`, `created_at`, `updated_at`) VALUES
(1, 2, 3, NULL, NULL),
(2, 2, 4, '2019-02-24 08:10:31', '2019-02-24 08:10:31');

-- --------------------------------------------------------

--
-- Table structure for table `diseases`
--

CREATE TABLE `diseases` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `common_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `specialty_id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `diseases`
--

INSERT INTO `diseases` (`id`, `code`, `name`, `common_name`, `specialty_id`, `active`, `created_at`, `updated_at`) VALUES
(3, 'Glaucoma', 'Glaucoma', 'المياه الزرقاء', 3, 1, '2019-02-22 00:12:29', '2019-02-22 00:12:29'),
(4, 'Myopia', 'Myopia', 'قصر النظر', 3, 1, '2019-02-22 00:13:04', '2019-02-22 00:13:04'),
(5, 'juoi', 'hfgdf', 'tgfd', 2, 1, '2019-02-25 13:19:00', '2019-02-25 13:19:00');

-- --------------------------------------------------------

--
-- Table structure for table `diseases_syptoms`
--

CREATE TABLE `diseases_syptoms` (
  `disease_id` int(10) UNSIGNED NOT NULL,
  `symptom_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `diseases_syptoms`
--

INSERT INTO `diseases_syptoms` (`disease_id`, `symptom_id`) VALUES
(5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` int(10) UNSIGNED NOT NULL,
  `dgree` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `dgree`, `ref_no`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'الأولى', '155847', 3, '2019-02-22 00:14:03', '2019-02-22 00:14:03'),
(2, 'الأولى', '955874', 4, '2019-02-22 00:15:15', '2019-02-22 00:15:15'),
(3, 'الأولى', '985774474', 5, '2019-02-23 01:35:09', '2019-02-23 01:35:09'),
(4, 'الأولى', '955874', 6, '2019-02-23 01:54:53', '2019-02-23 01:54:53'),
(5, 'الأولى', '65584', 10, '2019-02-23 02:24:18', '2019-02-23 02:24:18'),
(6, 'ثانية', '324234', 11, '2019-02-23 03:07:07', '2019-02-23 03:07:07'),
(7, '40', '98577', 12, '2019-02-25 09:43:40', '2019-02-25 09:43:40'),
(8, '40', '158478', 13, '2019-02-25 13:03:42', '2019-02-25 13:03:42');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_specialties`
--

CREATE TABLE `doctor_specialties` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `specialty_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctor_specialties`
--

INSERT INTO `doctor_specialties` (`id`, `user_id`, `specialty_id`, `created_at`, `updated_at`) VALUES
(2, 3, 6, NULL, NULL),
(3, 4, 6, NULL, NULL),
(6, 13, 2, NULL, NULL),
(7, 13, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `chat_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `chat_id`, `user_id`, `text`, `date`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'مساء خير مرحباً', '2019-02-22 00:00:00', NULL, NULL),
(2, 1, 3, 'مساء الخير', '2019-02-22 00:00:00', NULL, NULL),
(3, 1, 2, 'كيف الحال', '2019-02-22 06:23:44', '2019-02-22 04:23:44', '2019-02-22 04:23:44'),
(4, 1, 3, 'لا استطيع القدوم اليوم', '2019-02-22 06:29:14', '2019-02-22 04:29:14', '2019-02-22 04:29:14'),
(5, 1, 2, 'يمكن المجي غداً', '2019-02-22 06:29:32', '2019-02-22 04:29:32', '2019-02-22 04:29:32'),
(6, 2, 2, 'صباح الخير', '2019-02-24 10:10:44', '2019-02-24 08:10:44', '2019-02-24 08:10:44'),
(7, 2, 4, 'صباح النور', '2019-02-24 10:12:35', '2019-02-24 08:12:35', '2019-02-24 08:12:35'),
(8, 2, 4, 'السلام عليكم', '2019-02-25 11:55:06', '2019-02-25 09:55:06', '2019-02-25 09:55:06'),
(9, 2, 2, 'وعليكم السلام صباح الخير ابراهيم', '2019-02-25 11:55:50', '2019-02-25 09:55:50', '2019-02-25 09:55:50'),
(10, 2, 4, 'الحمد لله بخير وصحة', '2019-02-25 11:56:06', '2019-02-25 09:56:06', '2019-02-25 09:56:06'),
(11, 2, 2, 'وعليكم السلام صباح الخير ابراهيم', '2019-02-25 15:07:11', '2019-02-25 13:07:11', '2019-02-25 13:07:11');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2011_01_12_060934_create_nationalities_table', 1),
(2, '2013_02_17_091046_create_people_table', 1),
(3, '2013_02_18_090425_create_types_table', 1),
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2019_02_12_053326_create_specialties_table', 1),
(7, '2019_02_12_082539_create_symptoms_table', 1),
(8, '2019_02_12_085950_create_diseases_table', 1),
(9, '2019_02_12_104026_create_diseases_syptoms_table', 1),
(10, '2019_02_18_094938_create_registers_table', 1),
(11, '2019_02_21_221550_create_doctors_table', 1),
(12, '2019_02_21_225914_create_doctor_specialties_table', 1),
(13, '2019_02_22_044310_create_chats_table', 2),
(16, '2019_02_22_054929_create_messages_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `nationalities`
--

CREATE TABLE `nationalities` (
  `id` int(10) UNSIGNED NOT NULL,
  `nationality_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nationalities`
--

INSERT INTO `nationalities` (`id`, `nationality_name`, `created_at`, `updated_at`) VALUES
(1, 'ليبيj', NULL, '2019-02-25 13:20:33'),
(2, 'مصري', NULL, NULL),
(3, 'تونسي', NULL, NULL),
(4, 'أكراني', NULL, NULL),
(5, '51', '2019-02-25 13:19:59', '2019-02-25 13:19:59');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mid_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` tinyint(1) DEFAULT '1',
  `nationality_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`id`, `first_name`, `mid_name`, `last_name`, `family_name`, `date_of_birth`, `gender`, `nationality_id`, `created_at`, `updated_at`) VALUES
(1, 'منير', 'محمد', 'سالم', 'احمد', '2019-02-22', 0, 1, '2019-02-22 00:05:15', '2019-02-22 00:05:15'),
(2, 'منير', 'محمد', 'سالم', 'احمد', '2019-02-22', 0, 1, '2019-02-22 00:06:12', '2019-02-22 00:06:12'),
(3, 'سوسن', 'محمد', 'سالم', 'احمد', '2019-02-22', 1, 1, '2019-02-22 00:14:03', '2019-02-22 00:14:03'),
(4, 'ابراهيم', 'محمد', 'محمود', 'محمد', '2019-02-22', 0, 2, '2019-02-22 00:15:15', '2019-02-22 00:15:15'),
(5, 'ياسين', 'علي', 'محمد', 'ابراهيم', '2019-02-22', 0, 1, '2019-02-23 01:35:09', '2019-02-23 01:35:09'),
(6, 'ابراهيم', 'منير', 'سالم', 'احمد', '2019-02-15', 0, 1, '2019-02-23 01:54:53', '2019-02-23 01:54:53'),
(7, 'منير', 'محمد', 'سالم', 'احمد', '2019-02-15', 0, 1, '2019-02-23 02:13:41', '2019-02-23 02:13:41'),
(8, 'منير', 'محمد', 'سالم', 'احمد', '2019-02-15', 0, 1, '2019-02-23 02:14:22', '2019-02-23 02:14:22'),
(9, 'منير', 'محمد', 'سالم', 'احمد', '2019-02-15', 0, 1, '2019-02-23 02:14:57', '2019-02-23 02:14:57'),
(10, 'منير', 'محمد', 'سالم', 'احمد', '2019-02-15', 0, 1, '2019-02-23 02:19:30', '2019-02-23 02:19:30'),
(11, 'لطفي', 'صالح', 'ابراهيم', 'علي', '2019-02-14', 0, 2, '2019-02-23 02:24:18', '2019-02-23 02:24:18'),
(12, 'خليفة', 'علي', 'علي', 'محمد', '2019-02-16', 0, 1, '2019-02-23 03:07:07', '2019-02-23 03:07:07'),
(13, 'أشرف', 'محمد', 'سالم', 'الزريقي', '1977-02-15', 0, 1, '2019-02-25 09:43:39', '2019-02-25 09:43:39'),
(14, 'خوله', 'احمد', 'محمود', 'الزريقي', '2019-12-31', 0, 1, '2019-02-25 13:03:42', '2019-02-25 13:03:42');

-- --------------------------------------------------------

--
-- Table structure for table `registers`
--

CREATE TABLE `registers` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `specialties`
--

CREATE TABLE `specialties` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `specialties`
--

INSERT INTO `specialties` (`id`, `name_ar`, `name_en`, `active`, `created_at`, `updated_at`) VALUES
(2, 'القلب', 'heart', 1, '2019-02-22 00:07:10', '2019-02-22 00:07:10'),
(3, 'العيون', 'eyes', 1, '2019-02-22 00:07:29', '2019-02-22 00:07:29'),
(4, 'العظام', 'Bones', 1, '2019-02-22 00:07:52', '2019-02-22 00:07:52'),
(5, 'جراحة الاعصاب', 'Neurosurgery', 1, '2019-02-22 00:08:19', '2019-02-22 00:08:19'),
(6, 'امراض الباطنة', 'Abdominal diseases', 1, '2019-02-22 00:11:32', '2019-02-22 00:11:32'),
(7, '1562156', '6564', 1, '2019-02-25 13:15:22', '2019-02-25 13:15:22');

-- --------------------------------------------------------

--
-- Table structure for table `symptoms`
--

CREATE TABLE `symptoms` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `common_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `symptoms`
--

INSERT INTO `symptoms` (`id`, `name`, `common_name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'رشح', 'رشح', 1, '2019-02-22 00:09:02', '2019-02-22 00:09:02'),
(2, 'سعال', 'سعال', 1, '2019-02-22 00:09:13', '2019-02-22 00:09:13'),
(3, 'كحة', 'كحة', 1, '2019-02-22 00:09:22', '2019-02-22 00:09:22'),
(4, 'صداع', 'صداع', 1, '2019-02-22 00:09:34', '2019-02-22 00:09:34'),
(5, 'قي', 'قي', 1, '2019-02-22 00:09:46', '2019-02-22 00:09:46');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'مدير', NULL, NULL),
(2, 'طبيب', NULL, NULL),
(3, 'عضو', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `person_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `cover_image` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `type_id`, `person_id`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `active`, `cover_image`) VALUES
(2, 'admin', 1, 2, 'admin@admin.com', NULL, '$2y$10$RBQeN6zeW7fuB1Hsr8Joz./fW67ZPRquyzuxsub7VavnRqK9tES52', 'f5qh4swEsTxot87ZzjHVLkzWadrPF4hFqbHuMvn2TGxZik44rsU6kiGJH3Xt', '2019-02-22 00:06:12', '2019-02-22 00:06:12', 0, ''),
(3, 'سوسن', 2, 3, 'sawsan@admin.com', NULL, '$2y$10$4tgIxLNTXXHgffQujaUd6OviYPcAzFd6LxISgzrTcXcMEcu3JB1xi', 'ElYvz3nkapbMP3azfQ0QGEj95ffmG5yRgPpZNPzgXL4R5oa3xuSNNXxGmOKH', '2019-02-22 00:14:03', '2019-02-22 02:28:12', 1, ''),
(4, 'ابراهيم', 2, 4, 'ibrahim@admin.com', NULL, '$2y$10$Dsma0PGeUp2EpNsn.kFq1.heNxl5l/zhXp5XBg6pm2wYcgfC0knfK', 'TL4jn5V8SsNTHVukjZwCqUv0fGC7PoOVDH04nyKCTHgf42gBvE3bt2gmY2yM', '2019-02-22 00:15:15', '2019-02-22 02:28:21', 1, ''),
(5, 'ياسين', 2, 5, 'yassen@gamil.com', NULL, '$2y$10$RmVb4CYkIIuc.DpcWWUO3eNui2c1ZfIUhi9xhXqnqbWb7y.ufrGWy', NULL, '2019-02-23 01:35:09', '2019-02-23 01:47:44', 0, ''),
(6, 'ابراهيم', 2, 6, 'ibrahimre@admin.com', NULL, '$2y$10$oIjRAor0Q6pd6HrUoKvPeOhXMKB6A3a9ObXpfsdLBLDzB9IjPD67S', NULL, '2019-02-23 01:54:53', '2019-02-23 01:54:53', 0, ''),
(7, 'منير', 2, 8, 'rwefwef34we@admin.com', NULL, '$2y$10$Me5ZN7Tj6V47l335wXiNP.nPFnRJFkxykx0xnGd8kFHfQsu6uYWj.', NULL, '2019-02-23 02:14:22', '2019-02-23 02:14:22', 0, ''),
(8, 'منير', 2, 9, 'rwefwtef34we@admin.com', NULL, '$2y$10$vaMOqdHVEOg5iFXkCSk5DOcBwr9KzJAvoFADI.HjTjhd2KfIu8.5S', NULL, '2019-02-23 02:14:57', '2019-02-23 02:14:57', 0, ''),
(9, 'منير', 2, 10, 'rwefwtefy34we@admin.com', NULL, '$2y$10$0ypDJksno5znRRQy7BJQK.wNzvbRybDD0L7O26ZaUV.mwuIhLpR1W', NULL, '2019-02-23 02:19:30', '2019-02-23 02:19:30', 0, 'healthcare_647_1550895570.jpeg'),
(10, 'لطفي', 2, 11, 'lotfi@admin.com', NULL, '$2y$10$75mroSVDCMnEVBQki3J4MeEb/z.Qr/nfWS.k1Z3Ii2MUczllhmISC', NULL, '2019-02-23 02:24:18', '2019-02-23 02:24:18', 0, 'healthcare_647_1550895858.jpeg'),
(11, 'خليفة', 2, 12, 'kk@admin.com', NULL, '$2y$10$5MGfS/8P.F5DAudDV3VAg.I05ygG1CCK0lQJeEafJH1eu/ye/WaIO', NULL, '2019-02-23 03:07:07', '2019-02-23 03:07:07', 0, 'logo1_1550898427.jpg'),
(12, 'أشرف', 2, 13, 'ashref@gmail.com', NULL, '$2y$10$5J/Mn8u5Pns3f2AcVKKBT.YSY2YVRFx8Tjpl8i562lXoNvZgzVVfe', NULL, '2019-02-25 09:43:40', '2019-02-25 09:43:40', 0, 'photo_1551095019.png'),
(13, 'خوله', 2, 14, 'k@k.com', NULL, '$2y$10$LlGnwA61SePZDb79Ly3E0eVPctXSC.epqmrvnbydID/EHZ97bhFAS', 'vuQSfCFfRM6Fw6s10xBe3SH9ojt05UsoNJ8U3Ew1zbJvm1TcmCtCdfTHaefA', '2019-02-25 13:03:42', '2019-02-25 13:05:38', 1, '52816683_1997057017269172_3830223427659104256_n_1551107022.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chats_sender_id_index` (`sender_id`),
  ADD KEY `chats_received_id_index` (`received_id`);

--
-- Indexes for table `diseases`
--
ALTER TABLE `diseases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diseases_specialty_id_index` (`specialty_id`);

--
-- Indexes for table `diseases_syptoms`
--
ALTER TABLE `diseases_syptoms`
  ADD KEY `diseases_syptoms_disease_id_index` (`disease_id`),
  ADD KEY `diseases_syptoms_symptom_id_index` (`symptom_id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doctors_user_id_index` (`user_id`);

--
-- Indexes for table `doctor_specialties`
--
ALTER TABLE `doctor_specialties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doctor_specialties_user_id_index` (`user_id`),
  ADD KEY `doctor_specialties_specialty_id_index` (`specialty_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messages_chat_id_index` (`chat_id`),
  ADD KEY `messages_user_id_index` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nationalities`
--
ALTER TABLE `nationalities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_nationality_id_index` (`nationality_id`);

--
-- Indexes for table `registers`
--
ALTER TABLE `registers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specialties`
--
ALTER TABLE `specialties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `symptoms`
--
ALTER TABLE `symptoms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_type_id_index` (`type_id`),
  ADD KEY `users_person_id_index` (`person_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `diseases`
--
ALTER TABLE `diseases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `doctor_specialties`
--
ALTER TABLE `doctor_specialties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `nationalities`
--
ALTER TABLE `nationalities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `people`
--
ALTER TABLE `people`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `registers`
--
ALTER TABLE `registers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `specialties`
--
ALTER TABLE `specialties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `symptoms`
--
ALTER TABLE `symptoms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chats`
--
ALTER TABLE `chats`
  ADD CONSTRAINT `chats_received_id_foreign` FOREIGN KEY (`received_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `chats_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `diseases`
--
ALTER TABLE `diseases`
  ADD CONSTRAINT `diseases_specialty_id_foreign` FOREIGN KEY (`specialty_id`) REFERENCES `specialties` (`id`);

--
-- Constraints for table `diseases_syptoms`
--
ALTER TABLE `diseases_syptoms`
  ADD CONSTRAINT `diseases_syptoms_disease_id_foreign` FOREIGN KEY (`disease_id`) REFERENCES `diseases` (`id`),
  ADD CONSTRAINT `diseases_syptoms_symptom_id_foreign` FOREIGN KEY (`symptom_id`) REFERENCES `symptoms` (`id`);

--
-- Constraints for table `doctors`
--
ALTER TABLE `doctors`
  ADD CONSTRAINT `doctors_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `doctor_specialties`
--
ALTER TABLE `doctor_specialties`
  ADD CONSTRAINT `doctor_specialties_specialty_id_foreign` FOREIGN KEY (`specialty_id`) REFERENCES `specialties` (`id`),
  ADD CONSTRAINT `doctor_specialties_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_chat_id_foreign` FOREIGN KEY (`chat_id`) REFERENCES `chats` (`id`),
  ADD CONSTRAINT `messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `people`
--
ALTER TABLE `people`
  ADD CONSTRAINT `people_nationality_id_foreign` FOREIGN KEY (`nationality_id`) REFERENCES `nationalities` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `users_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

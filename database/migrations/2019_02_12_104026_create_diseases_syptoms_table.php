<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiseasesSyptomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diseases_syptoms', function (Blueprint $table) {

            $table->integer('disease_id')->unsigned()->index();
            $table->foreign('disease_id')->references('id')->on('diseases');

            $table->integer('symptom_id')->unsigned()->index();
            $table->foreign('symptom_id')->references('id')->on('symptoms');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diseases_syptoms');
    }
}

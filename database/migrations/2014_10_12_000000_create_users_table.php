<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
                        
            $table->integer('type_id')->unsigned()->index()->default(1);
            $table->foreign('type_id')->references('id')->on('types');
                        
            $table->integer('person_id')->unsigned()->index()->default(1);
            $table->foreign('person_id')->references('id')->on('people');

            $table->boolean('active')->default(false);

            $table->string('cover_image')->nullable();

            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('first_name', 50);
            $table->string('mid_name', 50);
            $table->string('last_name', 50);
            $table->string('family_name', 50);

            $table->date('date_of_birth');

            $table->boolean('gender')->nullable()->default(true);
            
            $table->integer('nationality_id')->unsigned()->index();
            $table->foreign('nationality_id')->references('id')->on('nationalities');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}

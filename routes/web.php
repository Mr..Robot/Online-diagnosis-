<?php

use App\Specialty;
use App\Symptom;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Doctor;
use App\Disease;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/',function(){
//     return view ('blank');
// });

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@dash')->name('home');
Auth::routes(['verify' => true]);


Route::get('/profile', 'ProfileController@show')->name('profile.show');
Route::get('/profile/{id}', 'ProfileController@find')->name('profile.find');
Route::post('/profile', 'ProfileController@promotion')->name('profile.promotion');
//Route::get('/profile', 'ProfileController@edit')->name('profile.edit');
Route::put('/profile', 'ProfileController@update')->name('profile.update');
Route::resource('doctor_work', 'DoctorWorkController');//->name('doctor_work');
Route::resource('doctor_certificates', 'DoctorCertificatesController');

Route::get('search', 'HomeController@view')->name('search');
// Route::post('search', 'HomeController@search')->name('search');

//nationalites route
Route::resource('nationalities', 'NationalityController');

//specialties route
Route::resource('specialties', 'SpecialtyController');
Route::get('specialties/{id}/changeState', 'SpecialtyController@changeState')->name('specialties.changeState');

//specialties route
Route::resource('symptoms', 'SymptomController');
Route::get('symptoms/{id}/changeState', 'SymptomController@changeState')->name('symptoms.changeState');

//specialties route
Route::resource('diseases', 'DiseaseController');
Route::get('diseases/{id}/changeState', 'DiseaseController@changeState')->name('diseases.changeState');

Route::resource('register', 'RegisterController');
Route::get('register_doctor', 'RegisterController@create_doctor')->name('register_doctor');
Route::post('store_doctor', 'RegisterController@store_doctor')->name('store_doctor');

Route::resource('doctor_specialties', 'DoctorSpecialtyController');


Route::resource('doctors', 'DoctorController');
Route::get('doctors/{id}/activate', 'DoctorController@activate')->name('doctors.activate');



Route::resource('chats', 'ChatController');
Route::get('secialaties/{id}', function ($id) {
    $spec =  Specialty::find($id);
    
    $symptoms = collect(new Symptom());
    
    foreach ($spec->diseases as $des) {
        $symptoms = $symptoms->merge($des->symptoms);
    }    
    $uniqueCollection = $symptoms->unique('id'); 
    return $uniqueCollection;
})->name('load-spec');

Route::post('search', function (Request $request) {
    $syms =  Symptom::where('id',[$request->symptomsSelect])->get();
    $diseases = collect(new Disease());

    foreach ($syms as $key1 => $sym) {
        foreach ($sym->diseases as $key2 => $dis) {
            if($dis->id == $request->pecialty_id){
                if(!$diseases->contains($dis)){
                    $diseases->prepend($dis);
                }
            }
        }
        
    }
    
    $spec =  Specialty::find($request->pecialty_id);
    
    $specialties = Specialty::all();

    // foreach ($spec->users as $key => $user) {
    //     $doctors = $user;
    // }

    // $symptoms  = Symptom::all()->pluck('common_name')->toArray();

    // $symptoms = implode(",",$symptoms);
  
    $data = [
        'doctors' => $spec->users,
        'specialties' => $specialties,
        'symptoms' => $syms ,
         'diseases' => $spec->diseases,
    ];

 //   dd()
    return view('search')->with( $data);
})->name('search');